package com.stikombali.fundamentalachievements.helper;

import android.view.View;


public interface ItemClickListener {
    void onClick(View view, int position, boolean isLongClick);
}
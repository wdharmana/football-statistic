package com.stikombali.fundamentalachievements.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.ActivityCompat;

/**
 * Created by dharmana on 5/20/17.
 */

public class Utils {

    /**
     * Checks if the app has permission to write to device storage
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    public static void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        String[] PERMISSIONS_STORAGE = {
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        };

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity, PERMISSIONS_STORAGE, 1
            );
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }


    public static String convertPositionNumber(String name, String team_pos) {
        String res = "";
        String num = "", order = "";

        if (name.equals("11")) {
            res = "GK";
        } else {

            Integer first = Integer.valueOf(name.substring(0, 1));
            Integer sec = Integer.valueOf(name.substring(1));

            String ftext = "", stext = "";


            switch (first) {
                case 2:
                    ftext = "B";
                    break;
                case 3:
                    ftext = "M";
                    break;
                case 4:
                    ftext = "F";
                    break;
            }

            if (sec == 2 || sec == 3) {
                stext = "C";
            }

            if (team_pos.equals("left")) {

                if (sec == 1) {
                    stext = "L";
                } else if (sec == 4) {
                    stext = "R";
                }
            } else if (team_pos.equals("right")) {
                if (sec == 4) {
                    stext = "L";
                } else if (sec == 1) {
                    stext = "R";
                }
            }

                /* define */
            res = ftext + stext;

        }
        return res;
    }


}

package com.stikombali.fundamentalachievements;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.stikombali.fundamentalachievements.adapter.PlayerAdapter;
import com.stikombali.fundamentalachievements.adapter.PlayerMatchAdapter;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.DividerItemDecoration;
import com.stikombali.fundamentalachievements.model.Player;

import java.util.ArrayList;
import java.util.List;

public class PlayerActivity extends AppCompatActivity implements PlayerAdapter.ClickListener, PlayerMatchAdapter.ClickListener {
    RecyclerView rv;
    DBConfig dbConfig;
    PlayerAdapter playerAdapter;
    PlayerMatchAdapter playerMatchAdapter;
    String v_player_id, v_team_name, action, cur_player, mode, player_old, v_position, v_match_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        v_team_name = getIntent().getStringExtra("v_team_name");

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        if (v_team_name != null) {
            getSupportActionBar().setSubtitle(v_team_name);
        }

        v_player_id = getIntent().getStringExtra("v_team_id");
        v_match_id = getIntent().getStringExtra("v_match_id");
        v_position = getIntent().getStringExtra("v_position");
        action = getIntent().getStringExtra("action");

        cur_player = getIntent().getStringExtra("cur_player");

        mode = getIntent().getStringExtra("mode");
        player_old = getIntent().getStringExtra("player_old");

        try {
            if (action.equals("pick_player")) {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            }
        } catch (Exception e) {

        }


        rv = (RecyclerView) findViewById(R.id.rv);
        dbConfig = new DBConfig(this);
        playerAdapter = new PlayerAdapter(this);

        String teamPos = "";
        if (v_match_id != null) {
            Integer intPos = dbConfig.checkSide(v_match_id, v_player_id);

            if (intPos == 1) {
                teamPos = "left";
            } else {
                teamPos = "right";
            }
            playerMatchAdapter = new PlayerMatchAdapter(this, teamPos);
        }

        getData();
        setMainRecyclerView();

    }


    public void getData() {
        List<Player> data = new ArrayList<>();
        Player mainInfo = null;

        Cursor c = dbConfig.getPlayerDataWhere(v_player_id, cur_player);
        if (c != null) {
            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex("name");
                String nameText = c.getString(nameIndex);
                String idText = c.getString(c.getColumnIndex("id"));
                String playerNumberText = c.getString(c.getColumnIndex("player_number"));
                String accountNumberText = c.getString(c.getColumnIndex("account_number"));
                String nicknameText = c.getString(c.getColumnIndex("nickname"));
                String statusText = c.getString(c.getColumnIndex("status"));
                String positionText = c.getString(c.getColumnIndex("position"));


                Player player = new Player();
                player.setName(nameText);
                player.setId(idText);
                player.setPlayer_number(playerNumberText);
                player.setAccount_number(accountNumberText);
                player.setNickname(nicknameText);
                player.setStatus(statusText);
                player.setPosition(positionText);

                if (action.equals("pick_player")) {
                    playerMatchAdapter.addItem(player);
                } else {
                    playerAdapter.addItem(player);
                }


            }
        }

    }

    public void setMainRecyclerView() {

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        if (action.equals("pick_player")) {
            rv.setAdapter(playerMatchAdapter);
        } else {
            rv.setAdapter(playerAdapter);
        }

    }

    @Override
    public void onClick(int position) {

        if (action.equals("pick_player")) {
            Player selectedItem = playerMatchAdapter.getSelectedItem(position);
            // Build a result intent and post it back.
            Intent resultIntent = new Intent();
            resultIntent.putExtra("ac_player_id", selectedItem.getId());
            resultIntent.putExtra("ac_player_number", selectedItem.getPlayer_number());
            resultIntent.putExtra("ac_player_nickname", selectedItem.getNickname());
            resultIntent.putExtra("ac_player_mode", mode);
            resultIntent.putExtra("ac_player_old", player_old);
            resultIntent.putExtra("ac_player_team", v_player_id);
            //Log.e("log", selectedItem.getPlayer_number());
            setResult(RESULT_OK, resultIntent);
            finish();
        } else {
            Player selectedItem = playerAdapter.getSelectedItem(position);
            //Toast.makeText(getApplicationContext(), selectedItem.getName(), Toast.LENGTH_LONG).show();
            Intent playerIntent = new Intent(getApplicationContext(), PlayerEditActivity.class);
            playerIntent.putExtra("player_id", selectedItem.getId());
            playerIntent.putExtra("player_name", selectedItem.getName());
            playerIntent.putExtra("player_nickname", selectedItem.getNickname());
            playerIntent.putExtra("player_number", selectedItem.getPlayer_number());
            startActivity(playerIntent);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}

package com.stikombali.fundamentalachievements;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Toast;

import com.stikombali.fundamentalachievements.adapter.PlayerAdapter;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.DividerItemDecoration;
import com.stikombali.fundamentalachievements.model.Player;

import java.util.ArrayList;
import java.util.List;

public class MatchPlayerActivity extends AppCompatActivity implements PlayerAdapter.ClickListener {

    RecyclerView rv;
    DBConfig dbConfig;
    PlayerAdapter playerAdapter;
    String v_player_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_player);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        v_player_id = getIntent().getStringExtra("v_team_id");

        rv = (RecyclerView) findViewById(R.id.rv);
        dbConfig = new DBConfig(this);
        playerAdapter = new PlayerAdapter(this);
        getData();
        setMainRecyclerView();

    }


    public void getData() {
        List<Player> data = new ArrayList<>();
        Player mainInfo = null;
        Cursor c = dbConfig.getPlayerData(v_player_id);
        if (c != null) {
            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex("name");
                String nameText = c.getString(nameIndex);
                String idText = c.getString(c.getColumnIndex("id"));
                String playerNumberText = c.getString(c.getColumnIndex("player_number"));
                String accountNumberText = c.getString(c.getColumnIndex("account_number"));

                Player player = new Player();
                player.setName(nameText);
                player.setId(idText);
                player.setPlayer_number(playerNumberText);
                player.setAccount_number(accountNumberText);
                playerAdapter.addItem(player);

            }
        }

    }

    public void setMainRecyclerView() {

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(playerAdapter);
    }

    @Override
    public void onClick(int position) {
        Player selectedItem = playerAdapter.getSelectedItem(position);
        Toast.makeText(getApplicationContext(), selectedItem.getName(), Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

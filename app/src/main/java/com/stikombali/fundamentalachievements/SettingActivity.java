package com.stikombali.fundamentalachievements;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.database.online.RestManager;
import com.stikombali.fundamentalachievements.database.online.pojo.Base;
import com.stikombali.fundamentalachievements.database.online.pojo.BiodataPemain;
import com.stikombali.fundamentalachievements.database.online.pojo.GoalTiming;
import com.stikombali.fundamentalachievements.database.online.pojo.Match;
import com.stikombali.fundamentalachievements.database.online.pojo.MatchDetail;
import com.stikombali.fundamentalachievements.database.online.pojo.Team;
import com.stikombali.fundamentalachievements.database.online.pojo.Tournament;
import com.stikombali.fundamentalachievements.database.online.pojo.TournamentDetail;
import com.stikombali.fundamentalachievements.helper.CSVWriter;
import com.stikombali.fundamentalachievements.helper.Utils;

import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.stikombali.fundamentalachievements.helper.Utils.verifyStoragePermissions;

public class SettingActivity extends AppCompatActivity {
    DBConfig dbConfig;
    RestManager mManager;
    ProgressDialog mDialog;
    List<com.stikombali.fundamentalachievements.model.Team> modelTeam;
    private ListView mainListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        /* Allow activity to show indeterminate progressbar */
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        mManager = new RestManager();

        dbConfig = new DBConfig(this);


        mainListView = (ListView) findViewById(R.id.list);

        final String[] values = new String[]{"Clear Result", "Clear All", "Import Online"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);


        // Assign adapter to List
        mainListView.setAdapter(adapter);


        mainListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String o = values[position];
                //Toast.makeText(getBaseContext(),o,Toast.LENGTH_SHORT).show();
                switch (position) {
                    case 0:
                        clear("result");
                        break;
                    case 1:
                        clear("all");
                        break;
                    case 2:
                        try {

                            importOnline();
                        } catch (Exception e) {
                            runSnack("Error: " + e.getMessage());

                        }

                        break;
                }
            }


        });
    }

    private void importOnline() {

        if (getNetworkAvailability()) {

            mDialog = new ProgressDialog(SettingActivity.this);
            mDialog.setMessage("Importing Data..");
            mDialog.setCancelable(false);
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);


            mDialog.show();


            Call<Base> listCall = mManager.getDataService().importAndroid();

            listCall.enqueue(new Callback<Base>() {
                @Override
                public void onResponse(Call<Base> call, Response<Base> response) {

                    if (response.isSuccessful()) {
                        Base resp = response.body();
                        List<Team> importTeam = resp.getTeam();
                        List<Tournament> importTournament = resp.getTournament();
                        List<TournamentDetail> importTournamentDetail = resp.getTournamentDetail();
                        List<Match> importMatch = resp.getMatch();
                        List<BiodataPemain> importBiodataPemain = resp.getBiodataPemain();
                        List<MatchDetail> importMatchDetail = resp.getMatchDetail();
                        List<GoalTiming> importGoalTiming = resp.getGoalTiming();

                        ArrayList<Team> arrayTeam = new ArrayList<Team>();
                        ArrayList<Tournament> arrayTournament = new ArrayList<Tournament>();
                        ArrayList<TournamentDetail> arrayTournamentDetail = new ArrayList<TournamentDetail>();
                        ArrayList<Match> arrayMatch = new ArrayList<Match>();
                        ArrayList<BiodataPemain> arrayBiodataPemain = new ArrayList<BiodataPemain>();
                        ArrayList<MatchDetail> arrayMatchDetail = new ArrayList<MatchDetail>();
                        ArrayList<MatchDetail> arrayMatchPlayer = new ArrayList<MatchDetail>();
                        ArrayList<GoalTiming> arrayGoalTiming = new ArrayList<GoalTiming>();

                        if (!resp.equals("")) {



                                /* Fetch Team */
                            for (int i = 0; i < importTeam.size(); i++) {
                                Team curTeam = importTeam.get(i);
                                arrayTeam.add(new Team(curTeam.getIdTeam(), curTeam.getNamaTeam()));
                            }
                                /* Fetch Tournament */
                            for (int i = 0; i < importTournament.size(); i++) {
                                Tournament curTournament = importTournament.get(i);
                                arrayTournament.add(new Tournament(curTournament.getIdTurnamen(), curTournament.getNamaTurnamen()));
                            }

                                /* Fetch Tournament Detail */
                            for (int i = 0; i < importTournamentDetail.size(); i++) {
                                TournamentDetail curTournamentDetail = importTournamentDetail.get(i);
                                arrayTournamentDetail.add(new TournamentDetail(curTournamentDetail.getIdTurnamenDetail(),
                                        curTournamentDetail.getIdTurnamen(), curTournamentDetail.getIdTeam()));
                            }

                                /* Fetch Match */
                            for (int i = 0; i < importMatch.size(); i++) {
                                Match curMatch = importMatch.get(i);
                                arrayMatch.add(
                                        new Match(curMatch.getIdMatch(), curMatch.getIdTurnamen(),
                                                curMatch.getIdTeamA(), curMatch.getIdTeamB(), curMatch.getNamaPertandingan(),
                                                curMatch.getNamaTeamA(),
                                                curMatch.getNamaTeamB(), curMatch.getPeriodePertandinganAwal(), curMatch
                                                .getPeriodePertandinganAkhir())
                                );

                            }

                                /* Fetch Goal Timing */
                            for (int i = 0; i < importGoalTiming.size(); i++) {
                                GoalTiming curGoalTiming = importGoalTiming.get(i);
                                GoalTiming goalTiming = new GoalTiming();

                                goalTiming.setId(curGoalTiming.getId());
                                goalTiming.setIdMatch(curGoalTiming.getIdMatch());
                                goalTiming.setIdPemain(curGoalTiming.getIdPemain());
                                goalTiming.setIdTeam(curGoalTiming.getIdTeam());
                                goalTiming.setGoalMinutes(curGoalTiming.getGoalMinutes());
                                goalTiming.setTimeAndroid(curGoalTiming.getTimeAndroid());

                                arrayGoalTiming.add(goalTiming);

                            }

                                /* Fetch Biodata Pemain */
                            for (int i = 0; i < importBiodataPemain.size(); i++) {
                                BiodataPemain curBiodataPemain = importBiodataPemain.get(i);
                                arrayBiodataPemain.add(
                                        new BiodataPemain(curBiodataPemain.getIdPemain(), curBiodataPemain.getNISN(), curBiodataPemain.getNamaLengkap(), curBiodataPemain
                                                .getNamaPanggilan(), curBiodataPemain.getNoPunggung())
                                );

                            }

                                /* Fetch Match player */
                            for (int i = 0; i < importMatchDetail.size(); i++) {

                                MatchDetail curMatchPlayer = importMatchDetail.get(i);
                                MatchDetail matchPlayer = new MatchDetail();

                                matchPlayer.setIdMatchDetail(curMatchPlayer.getIdMatchDetail());
                                matchPlayer.setIdMatch(curMatchPlayer.getIdMatch());
                                matchPlayer.setIdTeam(curMatchPlayer.getIdTeam());
                                matchPlayer.setIdPemain(curMatchPlayer.getIdPemain());
                                matchPlayer.setNoPunggungMatchDetail(curMatchPlayer.getNoPunggungMatchDetail());
                                matchPlayer.setPosisi(curMatchPlayer.getPosisi());
                                matchPlayer.setStatus(curMatchPlayer.getStatus());

                                arrayMatchPlayer.add(matchPlayer);

                            }

                                /* Fetch Match detail */
                            for (int i = 0; i < importMatchDetail.size(); i++) {

                                MatchDetail curMatchDetail = importMatchDetail.get(i);
                                MatchDetail matchDetail = new MatchDetail();

                                matchDetail.setIdMatchDetail(curMatchDetail.getIdMatchDetail());
                                matchDetail.setIdMatch(curMatchDetail.getIdMatch());
                                matchDetail.setIdPemain(curMatchDetail.getIdPemain());
                                matchDetail.setNoPunggungMatchDetail(curMatchDetail.getNoPunggungMatchDetail());
                                matchDetail.setIntTouch(curMatchDetail.getIntTouch());
                                matchDetail.setIntFoulsPassing(curMatchDetail.getIntFoulsPassing());
                                matchDetail.setIntFoulsControl(curMatchDetail.getIntFoulsControl());
                                matchDetail.setIntShootOnTarget(curMatchDetail.getIntShootOnTarget());
                                matchDetail.setIntGoal(curMatchDetail.getIntGoal());
                                matchDetail.setYellowCard(curMatchDetail.getYellowCard());
                                matchDetail.setRedCard(curMatchDetail.getRedCard());
                                matchDetail.setFouls(curMatchDetail.getFouls());

                                matchDetail.setIntSaving(curMatchDetail.getIntSaving());
                                matchDetail.setIntDistribution(curMatchDetail.getIntDistribution());
                                matchDetail.setIntAnticipation(curMatchDetail.getIntAnticipation());

                                arrayMatchDetail.add(matchDetail);

                            }

                            dbConfig.generateOnline(arrayTeam, arrayTournament, arrayTournamentDetail, arrayMatch,
                                    arrayBiodataPemain, arrayMatchPlayer, arrayMatchDetail, arrayGoalTiming);

                            Toast.makeText(getApplicationContext(), "Data berhasil digenerate!", Toast.LENGTH_LONG)
                                    .show();
                        }
                    } else {

                        int sc = response.code();
                        switch (sc) {
                            case 400:
                                Log.e("Error 400", "Bad Request");
                                break;
                            case 404:
                                Log.e("Error 404", "Not Found");
                                break;
                            default:
                                Log.e("Error", "Generic Error");
                        }

                        Toast.makeText(getApplicationContext(), "Failed to import. Error code: " + sc, Toast.LENGTH_LONG)
                                .show();


                    }

                    mDialog.dismiss();

                }

                @Override
                public void onFailure(Call<Base> call, Throwable t) {
                    //Log.e("failed", t.getMessage());
                    mDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }


            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Mohon aktifkan koneksi internet anda.", Toast.LENGTH_LONG).show();
        }

    }


    public String recheckStr(String obj) {
        try {
            if (obj.equals("")) {
                Log.e("error", obj);
                return "0";

            } else {
                Log.e("no_error", obj);
                return obj;

            }

        } catch (Exception e) {
            Log.e("error", e.getMessage());
            return "0";
        }
    }


    public boolean getNetworkAvailability() {
        return Utils.isNetworkAvailable(getApplicationContext());
    }


    private void importDummy() {
        boolean empty = dbConfig.isEmpty();
        if (empty) {
            ProgressDialog mDialog = new ProgressDialog(this);
            mDialog.setMessage("Processing...");
            mDialog.setCancelable(true);
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);


            mDialog.show();

            try {

                dbConfig.importDummy();
                mDialog.dismiss();
                runSnack("Data dummy berhasil diimport ke dalam database aplikasi.");

            } catch (Exception e) {
                mDialog.dismiss();
            }
        } else {
            runSnack("Masih ada data dalam database. Silahkan kosongkan terlebih dahulu.");
        }
    }

    private void clear(final String mode) {
        String msg = "Anda yakin ingin menghapus seluruh data hasil statistik pertandingan? Data " +
                "tidak dapat dikembalikan setelah melakukan tindakan ini.";
        if (mode.equals("all")) {
            msg = "Anda yakin ingin menghapus seluruh data dalam aplikasi? Data " +
                    "tidak dapat dikembalikan setelah melakukan tindakan ini.";
        }
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Konfirmasi");
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setPositiveButton("YA", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dbConfig.clear(mode);
                if (mode.equals("result")) {
                    runSnack("Seluruh data hasil statistik pertandingan berhasil dihapus");
                } else {
                    runSnack("Seluruh data berhasil dikosongkan.");
                }

            }

        });

        alertDialogBuilder.setNeutralButton("BATAL", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }

        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    private void exportCSV() {
        verifyStoragePermissions(this);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String timestamp_str = String.valueOf(timestamp.getTime());
        File dbFile = getDatabasePath("_fs_db.db");
        DBConfig dbhelper = new DBConfig(getApplicationContext());
        File exportDir = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }
        String fileName = "EXPORT-ALL-" + timestamp_str + ".csv";
        File file = new File(exportDir, fileName);
        Uri uri = Uri.fromFile(file);
        //Log.e("URL PATH", String.valueOf(uri));
        try {
            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            Cursor curCSV = db.rawQuery("SELECT * FROM match_detail ORDER BY match_id ASC", null);
            csvWrite.writeNext(curCSV.getColumnNames());
            while (curCSV.moveToNext()) {
                //Which column you want to exprort
                String arrStr[] = {curCSV.getString(0), curCSV.getString(1), curCSV.getString(2)};
                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
            runSnack("File " + fileName + " berhasil diexport. File ini berlokasi di folder Download.");
        } catch (Exception sqlEx) {
            Log.e("SettingActivity", sqlEx.getMessage(), sqlEx);
        }
    }

    public void runSnack(String msg) {
        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

        snackbar.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

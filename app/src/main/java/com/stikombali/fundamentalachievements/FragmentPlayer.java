package com.stikombali.fundamentalachievements;


import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.stikombali.fundamentalachievements.adapter.PlayerMatchAdapter;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.DividerItemDecoration;
import com.stikombali.fundamentalachievements.model.Player;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentPlayer extends Fragment implements PlayerMatchAdapter.ClickListener {

    RecyclerView rv;
    DBConfig dbConfig;
    PlayerMatchAdapter playerAdapter;
    String v_team_id, v_team_name, v_pos, cur_player, v_match_id;

    public FragmentPlayer() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        v_team_id = args.getString("team");
        v_pos = args.getString("pos");
        v_match_id = args.getString("match_id");
        v_team_name = args.getString("v_team_name");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.content_player_match, container, false);


        rv = (RecyclerView) rootView.findViewById(R.id.rv);
        dbConfig = new DBConfig(getActivity());
        playerAdapter = new PlayerMatchAdapter(this, v_pos);
        getData();
        setMainRecyclerView();

        return rootView;
    }

    public void getData() {
        List<Player> data = new ArrayList<>();
        Player mainInfo = null;

        Cursor c = dbConfig.getPlayerByMatchTeam(v_team_id, v_match_id);
        if (c != null) {
            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex("name");
                String nameText = c.getString(nameIndex);
                String idText = c.getString(c.getColumnIndex("id"));
                String playerNumberText = c.getString(c.getColumnIndex("player_number"));
                String accountNumberText = c.getString(c.getColumnIndex("account_number"));
                String nicknameText = c.getString(c.getColumnIndex("nickname"));
                String statusText = c.getString(c.getColumnIndex("status"));
                String positionText = c.getString(c.getColumnIndex("position"));

                Player player = new Player();
                player.setName(nameText);
                player.setId(idText);
                player.setPlayer_number(playerNumberText);
                player.setAccount_number(accountNumberText);
                player.setNickname(nicknameText);
                player.setStatus(statusText);
                player.setPosition(positionText);
                playerAdapter.addItem(player);

            }
        }

    }

    public void setMainRecyclerView() {

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        rv.setAdapter(playerAdapter);
    }


    @Override
    public void onClick(int position) {
        Player selectedItem = playerAdapter.getSelectedItem(position);

        Intent playerIntent = new Intent(getActivity(), PlayerMatchEditActivity.class);
        playerIntent.putExtra("player_id", selectedItem.getId());
        playerIntent.putExtra("player_name", selectedItem.getName());
        playerIntent.putExtra("player_nickname", selectedItem.getNickname());
        playerIntent.putExtra("player_number", selectedItem.getPlayer_number());
        startActivity(playerIntent);
    }
}

package com.stikombali.fundamentalachievements;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.stikombali.fundamentalachievements.adapter.MatchAdapter;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.DividerItemDecoration;
import com.stikombali.fundamentalachievements.model.Match;

import java.util.ArrayList;
import java.util.List;

public class MatchListActivity extends AppCompatActivity implements MatchAdapter.ClickListener {

    RecyclerView rv;
    DBConfig dbConfig;
    MatchAdapter matchAdapter;
    String v_tournament_id;
    String action;
    Intent clickIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setSubtitle(getIntent().getStringExtra("v_tournament_name"));

        v_tournament_id = getIntent().getStringExtra("v_tournament_id");

        action = getIntent().getStringExtra("action");

        rv = (RecyclerView) findViewById(R.id.rv);
        dbConfig = new DBConfig(this);
        matchAdapter = new MatchAdapter(this);
        getData();
        setMainRecyclerView();


    }

    public void getData() {
        List<Match> data = new ArrayList<>();
        Match mainInfo = null;
        Cursor c = dbConfig.getMatchData(v_tournament_id);
        if (c != null) {
            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex("name");
                String nameText = c.getString(nameIndex);
                String idText = c.getString(c.getColumnIndex("id"));
                String teamaText = c.getString(c.getColumnIndex("team_a"));
                String teambText = c.getString(c.getColumnIndex("team_b"));
                String namateamaText = c.getString(c.getColumnIndex("nama_team_a"));
                String namateambText = c.getString(c.getColumnIndex("nama_team_b"));
                String startText = c.getString(c.getColumnIndex("periode_start"));

                Match match = new Match();
                match.setName(nameText);
                match.setId(idText);
                match.setPeriode_start(startText);
                match.setTeam_a(teamaText);
                match.setTeam_b(teambText);
                match.setNama_team_a(namateamaText);
                match.setNama_team_b(namateambText);
                matchAdapter.addItem(match);

            }
        }

    }

    public void setMainRecyclerView() {

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(matchAdapter);
    }

    @Override
    public void onClick(int position) {
        Match selectedItem = matchAdapter.getSelectedItem(position);
        if (action.equals("touch") || action.equals("fouls_passing") || action.equals("fouls_control") || action.equals
                ("sgc") || action.equals("keeper")) {

            clickIntent = new Intent(getApplicationContext(), MatchActivity.class);
            clickIntent.putExtra("match_id", selectedItem.getId());
            clickIntent.putExtra("match_name", selectedItem.getNama_team_a() + " VS " + selectedItem.getNama_team_b());
            clickIntent.putExtra("match_team_a", selectedItem.getTeam_a());
            clickIntent.putExtra("match_team_b", selectedItem.getTeam_b());
            clickIntent.putExtra("match_start", selectedItem.getPeriode_start());
            clickIntent.putExtra("action", action);
            clickIntent.putExtra("action_name", actionName(action));
            Log.e("log", selectedItem.getTeam_a());
            startActivity(clickIntent);
        } else {
            /*
            Toast.makeText(getApplicationContext(), selectedItem.getNama_team_a() + " VS " + selectedItem.getNama_team_b(), Toast
                    .LENGTH_LONG)
                    .show(); */
            Intent intent = new Intent(getApplicationContext(), PlayerMatchActivity.class);

            intent.putExtra("match_id", selectedItem.getId());
            intent.putExtra("match_name", selectedItem.getNama_team_a() + " VS " + selectedItem.getNama_team_b());
            intent.putExtra("match_team_a", selectedItem.getTeam_a());
            intent.putExtra("match_team_b", selectedItem.getTeam_b());
            intent.putExtra("match_start", selectedItem.getPeriode_start());

            startActivity(intent);
        }


    }

    public String actionName(String act) {
        String str = "";
        switch (act) {
            case "touch":
                str = "Touch Ball";
                break;
            case "fouls_passing":
                str = "False Passing";
                break;
            case "fouls_control":
                str = "False Control";
                break;
            case "sgc":
                str = "Shoot on Target, Goal & Card";
                break;
            case "keeper":
                str = "Goal Keeper";
                break;
        }
        return str;

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

package com.stikombali.fundamentalachievements.model;

/**
 * Created by dharmana on 4/9/17.
 */

public class Team {
    private String id;
    private String name;
    private String tournament_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTournament_id() {
        return tournament_id;
    }

    public void setTournament_id(String tournament_id) {
        this.tournament_id = tournament_id;
    }
}

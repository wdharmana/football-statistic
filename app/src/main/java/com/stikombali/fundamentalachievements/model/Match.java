package com.stikombali.fundamentalachievements.model;

/**
 * Created by dharmana on 4/9/17.
 */

public class Match {
    private String id;
    private String tournament_id;
    private String id_match;
    private String name;
    private String team_a, team_b, periode_start, periode_end, nama_team_a, nama_team_b;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTournament_id() {
        return tournament_id;
    }

    public void setTournament_id(String tournament_id) {
        this.tournament_id = tournament_id;
    }

    public String getId_match() {
        return id_match;
    }

    public void setId_match(String id_match) {
        this.id_match = id_match;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeam_a() {
        return team_a;
    }

    public void setTeam_a(String team_a) {
        this.team_a = team_a;
    }

    public String getTeam_b() {
        return team_b;
    }

    public void setTeam_b(String team_b) {
        this.team_b = team_b;
    }

    public String getPeriode_start() {
        return periode_start;
    }

    public void setPeriode_start(String periode_start) {
        this.periode_start = periode_start;
    }

    public String getPeriode_end() {
        return periode_end;
    }

    public void setPeriode_end(String periode_end) {
        this.periode_end = periode_end;
    }

    public String getNama_team_a() {
        return nama_team_a;
    }

    public void setNama_team_a(String nama_team_a) {
        this.nama_team_a = nama_team_a;
    }

    public String getNama_team_b() {
        return nama_team_b;
    }

    public void setNama_team_b(String nama_team_b) {
        this.nama_team_b = nama_team_b;
    }

}

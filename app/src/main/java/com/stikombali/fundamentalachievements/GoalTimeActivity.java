package com.stikombali.fundamentalachievements;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class GoalTimeActivity extends AppCompatActivity {

    EditText edtMinute;
    Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goal_time);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        edtMinute = (EditText) findViewById(R.id.edtMinute);
        btnUpdate = (Button) findViewById(R.id.btnUpdate);


        String dateInString = getIntent().getStringExtra("match_start");


        try {

            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
            Date date = df.parse(dateInString);
            Calendar cal = new GregorianCalendar();
            cal.setTime(date);
            Date calTime = cal.getTime();

            //Date d1 = new GregorianCalendar(2017, Calendar.JUNE, 4, 17, 11).getTime();

            Date today = new Date();

            long diff = today.getTime() - calTime.getTime();
            long diffMinutes = TimeUnit.MILLISECONDS.toMinutes(diff);

            Log.e("minutes:", String.valueOf(diffMinutes));

            if (diffMinutes < 0) {
                edtMinute.setText(0);
            } else {
                edtMinute.setText(String.valueOf(diffMinutes));
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("catch", e.getMessage());
        }

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                resultIntent.putExtra("goal_minute", edtMinute.getText().toString());
                resultIntent.putExtra("team_id", getIntent().getStringExtra("team_id"));
                resultIntent.putExtra("player", getIntent().getStringExtra("player"));
                resultIntent.putExtra("post_time", getIntent().getStringExtra("post_time"));
                setResult(RESULT_OK, resultIntent);
                finish();
            }
        });

    }


}

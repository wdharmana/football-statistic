package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dharmana on 6/2/17.
 */

public class Team {
    @SerializedName("idTeam")
    @Expose
    private String idTeam;
    @SerializedName("namaTeam")
    @Expose
    private String namaTeam;
    @SerializedName("alamatTeam")
    @Expose
    private String alamatTeam;

    public Team(String idTeam, String namaTeam) {
        setIdTeam(idTeam);
        setNamaTeam(namaTeam);
    }

    public String getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(String idTeam) {
        this.idTeam = idTeam;
    }

    public String getNamaTeam() {
        return namaTeam;
    }

    public void setNamaTeam(String namaTeam) {
        this.namaTeam = namaTeam;
    }

    public String getAlamatTeam() {
        return alamatTeam;
    }

    public void setAlamatTeam(String alamatTeam) {
        this.alamatTeam = alamatTeam;
    }

}

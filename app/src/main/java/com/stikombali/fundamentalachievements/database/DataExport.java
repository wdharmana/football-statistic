package com.stikombali.fundamentalachievements.database;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by dharmana on 6/7/17.
 */

public class DataExport {

    public String exportQuery(String match_id, String action) {
        String selAct = "", selWhere = "";
        if (action.equals("sgc")) {
            selAct = "match_detail.sot, match_detail.goal, match_detail.fouls, match_detail.yellow_card, match_detail" +
                    ".red_card";
        } else if (action.equals("keeper")) {
            selAct = "match_detail.reaction, match_detail.saving, match_detail.distribution, match_detail.anticipation";
        } else {
            selAct = "match_detail." + action;
        }

        if (action.equals("sgc") || action.equals("keeper")) {
            List<String> colList = Arrays.asList(selAct.split(","));
            String comma = "";
            for (int i = 0; i < colList.size(); i++) {
                if (i > 0) {
                    comma = " OR ";
                }
                selWhere += comma + colList.get(i) + " > 0";
            }

        } else {
            selWhere = selAct + " > 0";
        }

        String query = "SELECT match_player.id AS detail_id,match_player.team_id,match_player.player_number," +
                "match_detail.match_id, " + selAct + ", match_player.position, match_player.status " +
                "FROM match_detail " +
                "INNER JOIN match_player ON " +
                "match_detail.detail_id = match_player.id WHERE match_detail.match_id='" + match_id + "' AND " + selWhere;
        return query;
    }

    public String exportOnline(Context context, String match_id, String action) {
        DBConfig dbConfig = new DBConfig(context);

        Cursor cursor = dbConfig.customQuery(exportQuery(match_id, action));
        Cursor cursor2 = null;
        if (action.equals("sgc")) {
            cursor2 = dbConfig.customQuery("SELECT goal.*, match_player.player_number FROM goal INNER JOIN " +
                    "match_player ON goal.match_id = match_player.match_id" +
                    " WHERE " +
                    "goal.match_id='" + match_id + "'");
        }
        return cursorToString(cursor, cursor2);
    }

    public Cursor exportCSV(Context context, String match_id, String action) {
        DBConfig dbConfig = new DBConfig(context);
        if (!action.equals("goal")) {
            return dbConfig.customQuery(exportQuery(match_id, action));
        } else {
            return dbConfig.customQuery("SELECT * FROM goal WHERE match_id='" + match_id + "'");
        }

    }

    private String cursorToString(Cursor crs, Cursor cGoal) {

        JSONObject detailObj = new JSONObject();

        try {

            detailObj.put("matchDetail", fetchArray(crs));
            if (cGoal != null) {
                if (cGoal.getCount() > 0) {
                    detailObj.put("goalTiming", fetchArray(cGoal));
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return detailObj.toString();
    }


    public JSONArray fetchArray(Cursor crs) {

        JSONArray arr = new JSONArray();
        crs.moveToFirst();
        while (!crs.isAfterLast()) {
            int nColumns = crs.getColumnCount();
            JSONObject row = new JSONObject();
            for (int i = 0; i < nColumns; i++) {
                String colName = crs.getColumnName(i);
                if (colName != null) {
                    String val = "";
                    try {
                        switch (crs.getType(i)) {
                            case Cursor.FIELD_TYPE_BLOB:
                                row.put(colName, crs.getBlob(i).toString());
                                break;
                            case Cursor.FIELD_TYPE_FLOAT:
                                row.put(colName, crs.getDouble(i));
                                break;
                            case Cursor.FIELD_TYPE_INTEGER:
                                row.put(colName, crs.getLong(i));
                                break;
                            case Cursor.FIELD_TYPE_NULL:
                                row.put(colName, null);
                                break;
                            case Cursor.FIELD_TYPE_STRING:
                                row.put(colName, crs.getString(i));
                                break;
                        }
                    } catch (JSONException e) {
                        Log.e("err", e.getMessage());
                    }
                }
            }
            arr.put(row);


            if (!crs.moveToNext())
                break;
        }

        return arr;
    }
}

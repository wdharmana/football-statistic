package com.stikombali.fundamentalachievements.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.stikombali.fundamentalachievements.database.data.FormationData;
import com.stikombali.fundamentalachievements.database.data.MatchData;
import com.stikombali.fundamentalachievements.database.data.PlayerData;
import com.stikombali.fundamentalachievements.database.data.TeamData;
import com.stikombali.fundamentalachievements.database.data.TournamentData;
import com.stikombali.fundamentalachievements.database.online.pojo.BiodataPemain;
import com.stikombali.fundamentalachievements.database.online.pojo.GoalTiming;
import com.stikombali.fundamentalachievements.database.online.pojo.Match;
import com.stikombali.fundamentalachievements.database.online.pojo.MatchDetail;
import com.stikombali.fundamentalachievements.database.online.pojo.Team;
import com.stikombali.fundamentalachievements.database.online.pojo.Tournament;
import com.stikombali.fundamentalachievements.database.online.pojo.TournamentDetail;

import java.util.ArrayList;

/**
 * Created by dharmana on 4/9/17.
 */

public class DBConfig extends SQLiteOpenHelper {

    public static final String TOURNAMEN_TABLE_NAME = "tournament";
    public static final String TOURNAMEN_COL_ID = "id";
    public static final String TOURNAMEN_COL_NAME = "name";
    private static final int DATABASE_VERSION = 35;
    private static final String DATABASE_NAME = "_fs_db";
    private static final String CREATE_TOURNAMEN_TABLE = "CREATE TABLE " + TOURNAMEN_TABLE_NAME + "( id VARCHAR(27) PRIMARY KEY NOT " +
            "NULL, " +
            "name VARCHAR(200), record_status_id INTEGER)";

    private static final String CREATE_TOURNAMEN_DETAIL_TABLE = "CREATE TABLE tournament_detail ( id VARCHAR" +
            "(27) PRIMARY KEY NOT " +
            "NULL, " +
            "tournament_id VARCHAR(27) NOT NULL, team_id VARCHAR(27) NOT NULL)";

    private static final String CREATE_TEAM_TABLE = "CREATE TABLE team( id VARCHAR(27) NOT NULL PRIMARY KEY, team_id VARCHAR(20), tournament_id VARCHAR(27), " +
            "name VARCHAR(50), record_status_id VARCHAR(1))";
    private static final String CREATE_PLAYER_TABLE = "CREATE TABLE player( id VARCHAR(20) NOT NULL PRIMARY KEY, " +
            "name VARCHAR(50), nickname VARCHAR(15), player_number VARCHAR(4), account_number VARCHAR(15))";

    private static final String CREATE_MATCH_TABLE = "CREATE TABLE match(id VARCHAR(20) NOT NULL PRIMARY KEY, " +
            "tournament_id VARCHAR(27),name VARCHAR(100),  team_a VARCHAR(27), team_b " +
            "VARCHAR(27),nama_team_a VARCHAR(100), nama_team_b VARCHAR(100), periode_start VARCHAR(30), periode_end " +
            "VARCHAR(30),status INTEGER DEFAULT 0 )";

    private static final String CREATE_MATCH_PLAYER_TABLE = "CREATE TABLE match_player(id VARCHAR(20) NOT NULL " +
            "PRIMARY KEY, match_id VARCHAR(27),team_id VARCHAR(27), player_id VARCHAR(27),position VARCHAR(10), " +
            "player_number VARCHAR(4), " +
            "status INTEGER DEFAULT 0 )";

    private static final String CREATE_MATCH_DETAIL_TABLE = "CREATE TABLE match_detail(id INTEGER " +
            "PRIMARY KEY AUTOINCREMENT,detail_id INTEGER, " +
            "match_id VARCHAR(20),player_id VARCHAR(20) NOT NULL, touch INTEGER DEFAULT 0, fouls_passing INTEGER DEFAULT 0, " +
            "fouls_control INTEGER DEFAULT 0, reaction VARCHAR(20), saving INTEGER DEFAULT 0, distribution INTEGER " +
            "DEFAULT 0, anticipation " +
            "INTEGER DEFAULT 0, sot INTEGER DEFAULT 0, goal INTEGER DEFAULT 0,fouls INTEGER DEFAULT 0, red_card " +
            "INTEGER DEFAULT 0, yellow_card INTEGER DEFAULT 0)";

    private static final String CREATE_FORMATION_TABLE = "CREATE TABLE formation(id INTEGER " +
            "PRIMARY KEY AUTOINCREMENT," +
            "match_id VARCHAR(20),team_id VARCHAR(20), player_id VARCHAR(20) NOT NULL, position VARCHAR(10) NOT NULL )";

    private static final String CREATE_GOAL_TABLE = "CREATE TABLE goal(id INTEGER " +
            "PRIMARY KEY AUTOINCREMENT," +
            "match_id VARCHAR(20),team_id VARCHAR(20), player_id VARCHAR(20) NOT NULL, minute INTEGER DEFAULT 0, " +
            "post_time DATETIME )";

    private SQLiteDatabase database;

    public DBConfig(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TOURNAMEN_TABLE);
        db.execSQL(CREATE_TOURNAMEN_DETAIL_TABLE);
        db.execSQL(CREATE_TEAM_TABLE);
        db.execSQL(CREATE_PLAYER_TABLE);
        db.execSQL(CREATE_MATCH_TABLE);
        db.execSQL(CREATE_MATCH_PLAYER_TABLE);
        db.execSQL(CREATE_MATCH_DETAIL_TABLE);
        db.execSQL(CREATE_GOAL_TABLE);
        //generateData(db);
    }

    private void generateData(SQLiteDatabase db) {
        for (int i = 0; i < TournamentData.defaultData.length; i++) {

            ContentValues cva = new ContentValues();
            cva.put(TOURNAMEN_COL_ID, TournamentData.defaultData[i][0]);
            cva.put(TOURNAMEN_COL_NAME, TournamentData.defaultData[i][1]);
            db.insert(TOURNAMEN_TABLE_NAME, null, cva);


        }
        for (int i = 0; i < TeamData.defaultData.length; i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", TeamData.defaultData[i][0]);
            cva.put("team_id", TeamData.defaultData[i][1]);
            cva.put("tournament_id", TeamData.defaultData[i][2]);
            cva.put("name", TeamData.defaultData[i][3]);
            db.insert("team", null, cva);


        }
        for (int i = 0; i < PlayerData.defaultData.length; i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", PlayerData.defaultData[i][0]);
            cva.put("team_id", PlayerData.defaultData[i][1]);
            cva.put("tournament_id", PlayerData.defaultData[i][2]);
            cva.put("name", PlayerData.defaultData[i][3]);
            cva.put("nickname", PlayerData.defaultData[i][4]);
            cva.put("birth_date", PlayerData.defaultData[i][5]);
            cva.put("player_number", PlayerData.defaultData[i][6]);
            cva.put("account_number", PlayerData.defaultData[i][7]);
            db.insert("player", null, cva);

        }

        for (int i = 0; i < MatchData.defaultData.length; i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", MatchData.defaultData[i][0]);
            cva.put("tournament_id", MatchData.defaultData[i][1]);
            cva.put("id_match", MatchData.defaultData[i][2]);
            cva.put("name", MatchData.defaultData[i][3]);
            cva.put("team_a", MatchData.defaultData[i][4]);
            cva.put("team_b", MatchData.defaultData[i][5]);
            cva.put("periode_start", MatchData.defaultData[i][6]);
            cva.put("periode_end", MatchData.defaultData[i][7]);
            db.insert("match", null, cva);

        }


        for (int i = 0; i < FormationData.defaultData.length; i++) {
            ContentValues cva = new ContentValues();

            cva.put("match_id", FormationData.defaultData[i][0]);
            cva.put("team_id", FormationData.defaultData[i][1]);
            cva.put("player_id", FormationData.defaultData[i][2]);
            cva.put("position", FormationData.defaultData[i][3]);
            db.insert("formation", null, cva);

        }
    }

    public void generateOnline(ArrayList<Team> dataTeam, ArrayList<Tournament> dataTournament, ArrayList<TournamentDetail>
            dataTournamentDetail, ArrayList<Match> dataMatch, ArrayList<BiodataPemain> dataBiodataPemain,
                               ArrayList<MatchDetail> dataMatchPlayer,
                               ArrayList<MatchDetail> dataMatchDetail, ArrayList<GoalTiming> dataGoalTiming) {
        SQLiteDatabase db = getWritableDatabase();
        dropDb(db);
        onCreate(db);

        for (int i = 0; i < dataTeam.size(); i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", dataTeam.get(i).getIdTeam());
            cva.put("team_id", dataTeam.get(i).getIdTeam());
            cva.put("name", dataTeam.get(i).getNamaTeam());
            db.insert("team", null, cva);
        }

        for (int i = 0; i < dataTournament.size(); i++) {
            ContentValues cva = new ContentValues();
            cva.put(TOURNAMEN_COL_ID, dataTournament.get(i).getIdTurnamen());
            cva.put(TOURNAMEN_COL_NAME, dataTournament.get(i).getNamaTurnamen());
            db.insert(TOURNAMEN_TABLE_NAME, null, cva);
        }

        for (int i = 0; i < dataTournamentDetail.size(); i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", dataTournamentDetail.get(i).getIdTurnamenDetail());
            cva.put("team_id", dataTournamentDetail.get(i).getIdTeam());
            cva.put("tournament_id", dataTournamentDetail.get(i).getIdTurnamen());
            db.insert("tournament_detail", null, cva);
        }

        for (int i = 0; i < dataMatch.size(); i++) {
            Log.e("nama tim a", dataMatch.get(i).getNamaTeamA());
            ContentValues cva = new ContentValues();
            cva.put("id", dataMatch.get(i).getIdMatch());
            cva.put("name", dataMatch.get(i).getNamaPertandingan());
            cva.put("tournament_id", dataMatch.get(i).getIdTurnamen());
            cva.put("team_a", dataMatch.get(i).getIdTeamA());
            cva.put("team_b", dataMatch.get(i).getIdTeamB());
            cva.put("nama_team_a", dataMatch.get(i).getNamaTeamA());
            cva.put("nama_team_b", dataMatch.get(i).getNamaTeamB());
            cva.put("periode_start", dataMatch.get(i).getPeriodePertandinganAwal());
            cva.put("periode_end", dataMatch.get(i).getPeriodePertandinganAkhir());
            db.insert("match", null, cva);
        }

        for (int i = 0; i < dataBiodataPemain.size(); i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", dataBiodataPemain.get(i).getIdPemain());
            cva.put("account_number", dataBiodataPemain.get(i).getNISN());
            cva.put("name", dataBiodataPemain.get(i).getNamaLengkap());
            cva.put("nickname", dataBiodataPemain.get(i).getNamaPanggilan());
            cva.put("player_number", dataBiodataPemain.get(i).getNoPunggung());
            db.insert("player", null, cva);
        }

        for (int i = 0; i < dataGoalTiming.size(); i++) {
            ContentValues cva = new ContentValues();
            cva.put("id", dataGoalTiming.get(i).getId());
            cva.put("match_id", dataGoalTiming.get(i).getIdMatch());
            cva.put("team_id", dataGoalTiming.get(i).getIdTeam());
            cva.put("player_id", dataGoalTiming.get(i).getIdPemain());
            cva.put("minute", dataGoalTiming.get(i).getGoalMinutes());
            cva.put("post_time", dataGoalTiming.get(i).getTimeAndroid());
            db.insert("goal", null, cva);
        }

        for (int i = 0; i < dataMatchPlayer.size(); i++) {
            MatchDetail matchDetail = dataMatchPlayer.get(i);
            String pos;
            if (matchDetail.getPosisi().equals("")) {
                pos = "";
            } else {
                pos = convertPosition(matchDetail.getPosisi(), checkPos
                        (matchDetail.getIdMatch(), matchDetail.getIdTeam()));
            }


            ContentValues cva = new ContentValues();
            cva.put("id", matchDetail.getIdMatchDetail());
            cva.put("team_id", matchDetail.getIdTeam());
            cva.put("player_id", matchDetail.getIdPemain());
            cva.put("match_id", matchDetail.getIdMatch());
            cva.put("position", pos);
            cva.put("player_number", matchDetail.getNoPunggungMatchDetail());
            cva.put("status", matchDetail.getStatus());
            db.insert("match_player", null, cva);
        }

        for (int i = 0; i < dataMatchDetail.size(); i++) {
            MatchDetail matchDetail = dataMatchDetail.get(i);

            //Log.e("logging", "touch: "+matchDetail.getIntTouch());


            ContentValues cva = new ContentValues();
            cva.put("detail_id", matchDetail.getIdMatchDetail());
            cva.put("player_id", matchDetail.getIdPemain());
            cva.put("match_id", matchDetail.getIdMatch());
            cva.put("touch", recheckStr(matchDetail.getIntTouch()));
            cva.put("fouls_passing", recheckStr(matchDetail.getIntFoulsPassing()));
            cva.put("fouls_control", recheckStr(matchDetail.getIntFoulsControl()));
            cva.put("sot", recheckStr(matchDetail.getIntShootOnTarget()));
            cva.put("yellow_card", recheckStr(matchDetail.getYellowCard()));
            cva.put("red_card", recheckStr(matchDetail.getRedCard()));
            cva.put("fouls", recheckStr(matchDetail.getFouls()));
            cva.put("goal", recheckStr(matchDetail.getIntGoal()));
            cva.put("saving", recheckStr(matchDetail.getIntSaving()));
            cva.put("distribution", recheckStr(matchDetail.getIntDistribution()));
            cva.put("anticipation", recheckStr(matchDetail.getIntAnticipation()));
            db.insert("match_detail", null, cva);
        }
    }

    public String recheckStr(String obj) {
        try {
            Integer intVal = Integer.parseInt(obj);
            Log.e("no_error", String.valueOf(intVal));
            return String.valueOf(intVal);

        } catch (Exception e) {
            Log.e("error", e.getMessage());
            return "0";
        }
    }

    public String convertPosition(String name, String team_pos) {
        String res = "";
        String num = "", order = "";
        Integer bc = 0, mc = 0, fc = 0;

        if (name.equals("GK")) {
            num = "1";
            order = "1";
        } else if (name.equals("BL") || name.equals("ML") || name.equals("FL")) {
            if (team_pos.equals("left")) {
                num = "1";
            } else {
                num = "4";
            }

        } else if (name.equals("BR") || name.equals("MR") || name.equals("FR")) {
            if (team_pos.equals("left")) {
                num = "4";
            } else {
                num = "1";
            }
        } else if (name.equals("BC")) {
            order = "2";
            if (bc.equals(0)) {
                num = "2";
            } else {
                num = "3";
            }
            bc++;
        } else if (name.equals("MC")) {
            order = "3";
            if (mc.equals(0)) {
                num = "2";
            } else {
                num = "3";
            }
            mc++;
        } else if (name.equals("FC")) {
            order = "4";
            if (fc.equals(0)) {
                num = "2";
            } else {
                num = "3";
            }
            fc++;
        }

        /* Fetch order */
        if (name.equals("BL") || name.equals("BR")) {
            order = "2";
        } else if (name.equals("ML") || name.equals("MR")) {
            order = "3";
        } else if (name.equals("FL") || name.equals("FR")) {
            order = "4";
        }


        res = order + "" + num;

        return res;
    }

    private void dropDb(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS tournament");
        db.execSQL("DROP TABLE IF EXISTS tournament_detail");
        db.execSQL("DROP TABLE IF EXISTS team");
        db.execSQL("DROP TABLE IF EXISTS player");
        db.execSQL("DROP TABLE IF EXISTS match");
        db.execSQL("DROP TABLE IF EXISTS match_player");
        db.execSQL("DROP TABLE IF EXISTS match_detail");
        //db.execSQL("DROP TABLE IF EXISTS formation");
        db.execSQL("DROP TABLE IF EXISTS goal");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        dropDb(db);
        //db.execSQL();
        onCreate(db);
    }

    public void importDummy() {
        database = getWritableDatabase();
        generateData(database);
    }

    public boolean isEmpty() {
        database = getWritableDatabase();
        Cursor c = database.rawQuery("SELECT * FROM player", null);
        return c.getCount() <= 0;
    }

    public Cursor customQuery(String query) {
        database = getReadableDatabase();
        Cursor c = database.rawQuery(query, null);
        return c;
    }

    public Integer getStatCount() {
        database = getWritableDatabase();
        Cursor c = database.rawQuery("SELECT * FROM match_detail", null);

        return c.getCount();
    }

    public void setMatchDetailData(String type, String match, String player) {
        database = getWritableDatabase();

        ContentValues cv = new ContentValues();
        Cursor c = getMD("*", match, player);

        Integer intMd = c.getCount();
        Integer lastVal;
        String where;

        if (intMd > 0) {

            c.moveToFirst();
            where = "id='" + c.getString(0) + "'";
            lastVal = getMDVal(type, match, player) + 1;
            cv.put(type, lastVal);
            database.update("match_detail", cv, where, null);
            Log.e("LOG", "UPDATE MATCH DETAIL=> " + type + ":" + lastVal);
        } else {
            String detail = getDetailId(match, player);
            lastVal = 1;
            cv.put("match_id", match);
            cv.put("player_id", player);
            cv.put("detail_id", detail);
            cv.put(type, lastVal);
            database.insert("match_detail", null, cv);
            Log.e("LOG", "INSERT MATCH DETAIL");
        }


    }

    private String getDetailId(String match, String player) {
        database = getReadableDatabase();

        String query = "SELECT id FROM match_player WHERE match_id='" + match + "' AND player_id='" + player + "'";

        Log.e("LOG", query);
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        String id = c.getString(0);
        return id;
    }

    public boolean updatePlayer(String id, String name, String nickname, String number) {
        database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        String where;
        where = "id='" + id + "'";
        cv.put("name", name);
        cv.put("nickname", nickname);
        cv.put("player_number", number);
        try {
            database.update("player", cv, where, null);
            return true;
        } catch (Exception e) {
            return false;
        }


    }

    public boolean updatePlayerMatch(String id, String number) {
        database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        String where;
        where = "id='" + id + "'";
        cv.put("player_number", number);
        try {
            database.update("match_player", cv, where, null);
            return true;
        } catch (Exception e) {
            return false;
        }


    }

    public boolean updateMatchStatus(String id) {
        database = getWritableDatabase();

        ContentValues cv = new ContentValues();

        String where;
        where = "id='" + id + "'";
        cv.put("status", 1);
        try {
            database.update("match", cv, where, null);
            return true;
        } catch (Exception e) {
            return false;
        }


    }

    public String getPlayerId(String team, String match, String playernumber) {
        database = getReadableDatabase();

        //String query = "SELECT player.id FROM player WHERE team_id='"+team+"' AND player_number='"+playernumber+"'";

        String query = "SELECT player.id FROM match_player " +
                "INNER JOIN player ON " +
                "match_player.player_id = player.id WHERE  " +
                "match_player.match_id='" + match + "' AND " +
                "match_player.team_id='" + team + "' AND match_player.player_number='" + playernumber + "'";

        Log.e("LOG", query);
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        String id = c.getString(0);
        return id;
    }

    public Cursor getPlayerDetail(String team, String match, String playernumber) {
        database = getReadableDatabase();

        String pid = getPlayerId(team, match, playernumber);

        String query = "SELECT * FROM player WHERE id='" + pid + "'";
        Log.e("LOG", query);
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();
        return c;
    }

    public int getMDVal(String type, String match, String player) {
        database = getReadableDatabase();
        String param = "*";
        Integer lastVal = 0;
        param = type;


        Cursor c = database.rawQuery("SELECT " + type + " FROM match_detail WHERE match_id='" + match + "' AND " +
                        "player_id='" + player + "'",
                null);
        Log.e("valcount", String.valueOf(c.getCount()));

        try {
            if (c.getCount() > 0) {

                c.moveToFirst();
                String paramIdx = c.getString(0);
                lastVal = Integer.parseInt(paramIdx);
            }
        } catch (Exception e) {
            Log.e("type", type);
        }

        return lastVal;
    }

    public Cursor getFormation(String match, String team) {
        database = getReadableDatabase();
        String query = "SELECT match_player.*, player.nickname FROM match_player " +
                "INNER JOIN player ON match_player.player_id = player.id WHERE  match_player" +
                ".match_id='" + match + "' AND " +
                "match_player.team_id='" + team + "' AND match_player.status=1";
        Cursor c = database.rawQuery(query, null);

        return c;
    }

    public Cursor getMD(String param, String match, String player) {
        database = getReadableDatabase();
        String query = "SELECT " + param + " FROM match_detail WHERE match_id='" + match + "' AND player_id='" + player + "'";
        Cursor c = database.rawQuery(query, null);

        return c;
    }

    public Integer checkSide(String match, String team) {
        database = getReadableDatabase();
        String query = "SELECT team_a, team_b FROM match WHERE id='" + match + "'";
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();

        if (c.getString(0).equals(team)) {
            return 1;
        } else {
            return 2;
        }

    }

    public boolean checkMatchStatus(String match) {
        database = getReadableDatabase();
        String query = "SELECT status FROM match WHERE id='" + match + "'";
        Cursor c = database.rawQuery(query, null);
        c.moveToFirst();

        return !c.getString(0).equals("0");

    }

    public void setTournamenData(String groupName, String groupNotes) {
        database = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(TOURNAMEN_COL_NAME, groupName);
        database.insert(TOURNAMEN_TABLE_NAME, null, cv);
    }


    public String checkPos(String match_id, String team_id) {
        Log.e("er", "match_id: " + match_id + " => " + team_id);
        database = getReadableDatabase();
        Cursor c = database.rawQuery("SELECT * FROM match WHERE id='" + match_id + "'", null);
        c.moveToFirst();

        String team_a = c.getString(c.getColumnIndex("team_a"));
        String team_b = c.getString(c.getColumnIndex("team_b"));

        if (team_id.equals(team_a)) {
            return "left";
        } else {
            return "right";
        }
    }

    public Cursor getTournamenData() {
        database = getReadableDatabase();
        String[] columns = {TOURNAMEN_COL_ID, TOURNAMEN_COL_NAME};
        Cursor c = database.query(TOURNAMEN_TABLE_NAME, columns, null, null, null, null, "id" + " DESC");
        return c;
    }

    public Cursor getTournamenName(String[] args) {
        database = getReadableDatabase();
        String query = "SELECT " + TOURNAMEN_COL_NAME + " FROM " + TOURNAMEN_TABLE_NAME + " WHERE " + TOURNAMEN_COL_NAME + " =?";
        Cursor c = database.rawQuery(query, args);
        return c;
    }

    public Cursor getTeamData(String team_id) {
        database = getReadableDatabase();
        String selectQuery = "SELECT  team.* FROM team INNER JOIN tournament_detail ON tournament_detail.team_id = " +
                "team.team_id WHERE  tournament_detail.tournament_id='" + team_id + "'";
        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public Cursor getMatchData(String turnamen) {
        database = getReadableDatabase();
        String selectQuery = "SELECT  * FROM match WHERE tournament_id='" + turnamen + "'";
        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public Cursor getPlayerData(String team) {
        database = getReadableDatabase();

        String selectQuery = "SELECT player.* FROM player INNER  JOIN match_player ON player.id = match_player" +
                ".player_id" +
                " WHERE match_player.team_id='" + team + "' GROUP BY match_player.player_id ORDER BY " +
                "player.player_number ASC ";
        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public Cursor getPlayerCadangan(String team, String match) {
        database = getReadableDatabase();

        String selectQuery = "SELECT player.id, player.name, player.nickname, player.account_number, match_player" +
                ".status, match_player.player_number FROM player " +
                "INNER  " +
                "JOIN match_player ON player.id " +
                "= match_player.player_id" +
                " WHERE match_player.team_id='" + team + "' AND match_player.match_id='" + match + "' AND status=0 GROUP BY" +
                " match_player.player_id ORDER BY " +
                "match_player.player_number ASC ";
        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public Cursor getPlayerDataWhere(String team, String cur_player) {
        database = getReadableDatabase();

        String where;
        String selectQuery = "";

        if (cur_player == null || cur_player.equals("")) {
            selectQuery = "SELECT player.id, player.name, player.nickname, player.account_number, match_player" +
                    ".player_number, match_player.status, match_player" +
                    ".position FROM " +
                    "player INNER " +
                    "JOIN " +
                    "match_player ON player.id = match_player" +
                    ".player_id" +
                    " WHERE  match_player.team_id='" + team + "' GROUP BY match_player" +
                    ".player_id";
        } else {
            where = " AND match_player.player_number NOT IN (" + cur_player + ")";
            selectQuery = "SELECT player.id, player.name, player.account_number, match_player.player_number, player" +
                    ".nickname, match_player.status, match_player.position " +
                    " FROM player INNER " +
                    "JOIN match_player ON player.id = match_player" +
                    ".player_id" +
                    " WHERE  match_player.team_id='" + team + "'" + where + " GROUP BY match_player" +
                    ".player_id";
        }

        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }


    public Cursor getPlayerByMatchTeam(String team, String match_id) {
        database = getReadableDatabase();

        String where;
        String selectQuery = "";

        selectQuery = "SELECT match_player.id,name,nickname,account_number, match_player.player_number as " +
                "player_number, match_player.status, match_player.position FROM " +
                "player INNER JOIN match_player ON player.id = match_player.player_id" +
                " WHERE  match_player.team_id='" + team + "' AND match_player.match_id='" + match_id + "' GROUP" +
                " BY " +
                "match_player.player_id ORDER BY match_player.status DESC";


        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public Cursor getStats(String type, String match, String team) {
        database = getReadableDatabase();
        String jumlah = type + " AS jumlah";
        String typeRange = " AND " + type + " >0 ";
        if (type.equals("sgc")) {
            jumlah = "sot, fouls, goal, yellow_card, red_card";
            typeRange = "";
        } else if (type.equals("keeper")) {
            jumlah = "reaction, saving, distribution,anticipation";
            typeRange = "";
        }


        String selectQuery = "SELECT player.id, player.name, player.nickname, player.account_number,match_player" +
                ".player_number," + jumlah + " FROM player INNER JOIN match_player ON player.id = " +
                "match_player.player_id" +
                " INNER JOIN " +
                "match_detail ON match_player.id = match_detail.detail_id  " +
                "WHERE match_player.team_id='" + team + "'" + " AND match_player.match_id='" + match + "' " + typeRange +
                "ORDER BY match_player.player_number ASC ";
        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public void updateFormation(String match, String team, String playerOld, String playerNew, String position) {
        playerOld = getPlayerId(team, match, playerOld);
        String where = "team_id='" + team + "' AND match_id='" + match + "' AND player_id='" + playerOld + "'";
        ContentValues cva = new ContentValues();
        cva.put("status", "0");
        cva.put("position", " ");
        database.update("match_player", cva, where, null);
        Log.e("QUERY", where);

        String where2 = "team_id='" + team + "' AND match_id='" + match + "' AND player_id='" + playerNew + "'";
        ContentValues cva2 = new ContentValues();
        cva2.put("status", "1");
        cva2.put("position", position);
        database.update("match_player", cva2, where2, null);

        Log.e("QUERY", where);
    }

    public void removeFormation(String match, String team, String player) {
        database = getReadableDatabase();
        String pid = getPlayerId(team, match, player);

        String query = "UPDATE match_player SET status=0 WHERE team_id='" + team + "' AND match_id='" + match + "' AND " +
                "player_id='" + pid + "'";
        database.execSQL(query);
        Log.e("QUERY", query);
    }

    public void insertFormation(String match, String team, String player, String position) {
        database = getReadableDatabase();

        //Log.e("cv", match+" - "+team+" - " +player+" - "+position);
        String query = "UPDATE match_player SET status=1,position='" + position + "' WHERE team_id='" + team + "' AND " +
                "match_id='" + match + "' AND player_id='" + player + "'";
        database.execSQL(query);
    }

    public void insertGoal(String match, String team, String player, String minute, String post_time) {
        database = getReadableDatabase();

        String query = "INSERT INTO goal(match_id, team_id, player_id, minute, post_time) VALUES('" + match + "', " +
                "'" + team + "','" + player + "','" + minute + "','" + post_time + "')";
        database.execSQL(query);
        Log.e("query", query);
    }

    public Cursor getGoal(String match, String team) {
        database = getReadableDatabase();
        String selectQuery = "SELECT goal.minute, player.name, match_player.player_number FROM goal INNER JOIN player" +
                " ON " +
                "player.id = goal.player_id INNER JOIN match_player ON match_player.player_id = player.id WHERE " +
                " match_player.match_id='" + match + "'  AND match_player.team_id='" +
                team + "'";
        Log.e("query", selectQuery);
        Cursor c = database.rawQuery(selectQuery, null);
        return c;
    }

    public void clear(String mode) {
        database = getReadableDatabase();
        if (mode.equals("result")) {
            database.execSQL("DELETE FROM match_detail");
        } else {
            database.execSQL("DELETE FROM match_player");
            database.execSQL("DELETE FROM match_detail");
            database.execSQL("DELETE FROM goal");
            database.execSQL("DELETE FROM player");
            database.execSQL("DELETE FROM match");
            database.execSQL("DELETE FROM tournament");
            database.execSQL("DELETE FROM team");
        }
    }

    public void setMatchDetailReaction(String selectedText, String match, String player) {
        database = getWritableDatabase();

        ContentValues cv = new ContentValues();
        Cursor c = getMD("*", match, player);

        Integer intMd = c.getCount();
        Integer lastVal;
        String where;

        if (intMd > 0) {

            c.moveToFirst();
            where = "id='" + c.getString(0) + "'";
            cv.put("reaction", selectedText);
            database.update("match_detail", cv, where, null);
            Log.e("LOG", "UPDATE MATCH DETAIL=> reaction:" + selectedText);
        } else {
            cv.put("match_id", match);
            cv.put("player_id", player);
            cv.put("reaction", selectedText);
            database.insert("match_detail", null, cv);
            Log.e("LOG", "INSERT MATCH DETAIL");
        }
    }
}

package com.stikombali.fundamentalachievements.database.data;

/**
 * Created by dharmana on 4/9/17.
 */

public class MatchData {

    public static String defaultData[][] = new String[][]{
            {"1", "1", "1", "Paradise Football Academy VS Putra Angkasa Kapal", "1", "2", "2017-05-27 10:00:00", "2017-05-27 11:00:00"},
            {"2", "1", "2", "SSB Badak Putra Buduk VS SSB Bali United", "3", "4", "2017-05-28 10:00:00", "2018-05-27" +
                    " 11:00:00"},
            {"3", "1", "3", "Paradise Football Academy VS SSB Badak Putra Buduk", "1", "3", "2017-05-27 10:00:00", "2017-05-27" +
                    " 11:00:00"},
            {"4", "1", "4", "Putra Angkasa Kapal VS SSB Bali United", "2", "4", "2017-05-28 10:00:00", "2018-05-27" +
                    " 11:00:00"},
    };
}
package com.stikombali.fundamentalachievements.database.data;

/**
 * Created by dharmana on 4/9/17.
 */

public class TeamData {

    public static String defaultData[][] = new String[][]{
            {"1", "1", "1", "Paradise Football Academy"},
            {"2", "2", "1", "Putra Angkasa Kapal"},
            {"3", "3", "1", "SSB Badak Putra Buduk"},
            {"4", "4", "1", "SSB Bali United"}
    };
}
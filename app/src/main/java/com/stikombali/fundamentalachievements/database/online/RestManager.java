package com.stikombali.fundamentalachievements.database.online;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dharmana on 6/2/17.
 */

public class RestManager {
    private DataService mDataService;

    public DataService getDataService() {
        if (mDataService == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("http://gobolabali.com/server/public/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            mDataService = retrofit.create(DataService.class);
        }
        return mDataService;
    }

}

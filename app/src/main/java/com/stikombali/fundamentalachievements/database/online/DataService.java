package com.stikombali.fundamentalachievements.database.online;

import com.stikombali.fundamentalachievements.database.online.pojo.Base;
import com.stikombali.fundamentalachievements.helper.Constants;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by dharmana on 6/2/17.
 */

public interface DataService {


    @GET("access_android")
    Call<Base> importAndroid();

    @FormUrlEncoded
    @POST("api/touch_sync/" + Constants.API_KEY)
    Call<String> syncTouch(@Field("data") String data);

    @FormUrlEncoded
    @POST("api/falsepassing_sync/" + Constants.API_KEY)
    Call<String> syncFalsePassing(@Field("data") String data);

    @FormUrlEncoded
    @POST("api/falsecontrol_sync/" + Constants.API_KEY)
    Call<String> syncFalseControl(@Field("data") String data);

    @FormUrlEncoded
    @POST("api/goalkeeper_sync/" + Constants.API_KEY)
    Call<String> syncGoalKeeper(@Field("data") String data);

    @FormUrlEncoded
    @POST("api/othervalue_sync/" + Constants.API_KEY)
    Call<String> syncOtherValue(@Field("data") String data);


}

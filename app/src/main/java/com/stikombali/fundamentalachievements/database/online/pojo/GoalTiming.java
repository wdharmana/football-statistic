package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dharmana on 6/15/17.
 */

public class GoalTiming {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("idMatch")
    @Expose
    private String idMatch;
    @SerializedName("idPemain")
    @Expose
    private String idPemain;
    @SerializedName("idTeam")
    @Expose
    private String idTeam;
    @SerializedName("noPunggung")
    @Expose
    private String noPunggung;
    @SerializedName("goal_minutes")
    @Expose
    private String goalMinutes;
    @SerializedName("TimeAndroid")
    @Expose
    private String timeAndroid;
    @SerializedName("PostDate")
    @Expose
    private String postDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(String idMatch) {
        this.idMatch = idMatch;
    }

    public String getIdPemain() {
        return idPemain;
    }

    public void setIdPemain(String idPemain) {
        this.idPemain = idPemain;
    }

    public String getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(String idTeam) {
        this.idTeam = idTeam;
    }

    public String getNoPunggung() {
        return noPunggung;
    }

    public void setNoPunggung(String noPunggung) {
        this.noPunggung = noPunggung;
    }

    public String getGoalMinutes() {
        return goalMinutes;
    }

    public void setGoalMinutes(String goalMinutes) {
        this.goalMinutes = goalMinutes;
    }

    public String getTimeAndroid() {
        return timeAndroid;
    }

    public void setTimeAndroid(String timeAndroid) {
        this.timeAndroid = timeAndroid;
    }

    public String getPostDate() {
        return postDate;
    }

    public void setPostDate(String postDate) {
        this.postDate = postDate;
    }

}

package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dharmana on 6/2/17.
 */

public class BiodataPemain {

    @SerializedName("idPemain")
    @Expose
    private String idPemain;
    @SerializedName("NISN")
    @Expose
    private String nISN;
    @SerializedName("namaLengkap")
    @Expose
    private String namaLengkap;
    @SerializedName("namaPanggilan")
    @Expose
    private String namaPanggilan;
    @SerializedName("tempatLahir")
    @Expose
    private String tempatLahir;
    @SerializedName("tglLahir")
    @Expose
    private String tglLahir;
    @SerializedName("jenisKelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("agama")
    @Expose
    private String agama;
    @SerializedName("kode_kabupaten")
    @Expose
    private String kodeKabupaten;
    @SerializedName("alamatRumah")
    @Expose
    private String alamatRumah;
    @SerializedName("boolWNI")
    @Expose
    private String boolWNI;
    @SerializedName("negara")
    @Expose
    private String negara;
    @SerializedName("noTlp")
    @Expose
    private String noTlp;
    @SerializedName("tinggiBadan")
    @Expose
    private String tinggiBadan;
    @SerializedName("beratBadan")
    @Expose
    private String beratBadan;
    @SerializedName("hobi")
    @Expose
    private String hobi;
    @SerializedName("hobiSejak")
    @Expose
    private String hobiSejak;
    @SerializedName("posisiFavorit")
    @Expose
    private String posisiFavorit;
    @SerializedName("namaSD")
    @Expose
    private String namaSD;
    @SerializedName("tahunSD")
    @Expose
    private String tahunSD;
    @SerializedName("namaSMP")
    @Expose
    private String namaSMP;
    @SerializedName("tahunSMP")
    @Expose
    private String tahunSMP;
    @SerializedName("namaSMA")
    @Expose
    private Object namaSMA;
    @SerializedName("tahunSMA")
    @Expose
    private String tahunSMA;
    @SerializedName("namaUniversitas")
    @Expose
    private String namaUniversitas;
    @SerializedName("tahunUniversitas")
    @Expose
    private String tahunUniversitas;
    @SerializedName("tlpSekolahTerakhir")
    @Expose
    private String tlpSekolahTerakhir;
    @SerializedName("anakKe")
    @Expose
    private String anakKe;
    @SerializedName("jumlahSaudara")
    @Expose
    private String jumlahSaudara;
    @SerializedName("namaAyah")
    @Expose
    private String namaAyah;
    @SerializedName("namaIbu")
    @Expose
    private String namaIbu;
    @SerializedName("pekerjaanAyah")
    @Expose
    private String pekerjaanAyah;
    @SerializedName("pekerjaanIbu")
    @Expose
    private String pekerjaanIbu;
    @SerializedName("airPerGelas")
    @Expose
    private String airPerGelas;
    @SerializedName("merkAir")
    @Expose
    private String merkAir;
    @SerializedName("susuPerHari")
    @Expose
    private String susuPerHari;
    @SerializedName("merkSusu")
    @Expose
    private String merkSusu;
    @SerializedName("vitamin")
    @Expose
    private String vitamin;
    @SerializedName("merkVitamin")
    @Expose
    private Object merkVitamin;
    @SerializedName("makanSayuran")
    @Expose
    private String makanSayuran;
    @SerializedName("makanBuah")
    @Expose
    private String makanBuah;
    @SerializedName("sarapan")
    @Expose
    private String sarapan;
    @SerializedName("foto")
    @Expose
    private String foto;
    @SerializedName("boolVerifikasi")
    @Expose
    private String boolVerifikasi;
    @SerializedName("tglVerifikasi")
    @Expose
    private String tglVerifikasi;
    @SerializedName("tglRegister")
    @Expose
    private String tglRegister;
    @SerializedName("NamaTeam")
    @Expose
    private String namaTeam;
    @SerializedName("thnTeam")
    @Expose
    private String thnTeam;
    @SerializedName("noPunggung")
    @Expose
    private String noPunggung;

    public BiodataPemain(String idPemain, String nISN, String namaLengkap, String namaPanggilan, String
            noPunggung) {
        setIdPemain(idPemain);
        setNISN(nISN);
        setNamaLengkap(namaLengkap);
        setNamaPanggilan(namaPanggilan);
        setNoPunggung(noPunggung);
    }

    public String getIdPemain() {
        return idPemain;
    }

    public void setIdPemain(String idPemain) {
        this.idPemain = idPemain;
    }

    public String getNISN() {
        return nISN;
    }

    public void setNISN(String nISN) {
        this.nISN = nISN;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNamaPanggilan() {
        return namaPanggilan;
    }

    public void setNamaPanggilan(String namaPanggilan) {
        this.namaPanggilan = namaPanggilan;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getKodeKabupaten() {
        return kodeKabupaten;
    }

    public void setKodeKabupaten(String kodeKabupaten) {
        this.kodeKabupaten = kodeKabupaten;
    }

    public String getAlamatRumah() {
        return alamatRumah;
    }

    public void setAlamatRumah(String alamatRumah) {
        this.alamatRumah = alamatRumah;
    }

    public String getBoolWNI() {
        return boolWNI;
    }

    public void setBoolWNI(String boolWNI) {
        this.boolWNI = boolWNI;
    }

    public String getNegara() {
        return negara;
    }

    public void setNegara(String negara) {
        this.negara = negara;
    }

    public String getNoTlp() {
        return noTlp;
    }

    public void setNoTlp(String noTlp) {
        this.noTlp = noTlp;
    }

    public String getTinggiBadan() {
        return tinggiBadan;
    }

    public void setTinggiBadan(String tinggiBadan) {
        this.tinggiBadan = tinggiBadan;
    }

    public String getBeratBadan() {
        return beratBadan;
    }

    public void setBeratBadan(String beratBadan) {
        this.beratBadan = beratBadan;
    }

    public String getHobi() {
        return hobi;
    }

    public void setHobi(String hobi) {
        this.hobi = hobi;
    }

    public String getHobiSejak() {
        return hobiSejak;
    }

    public void setHobiSejak(String hobiSejak) {
        this.hobiSejak = hobiSejak;
    }

    public String getPosisiFavorit() {
        return posisiFavorit;
    }

    public void setPosisiFavorit(String posisiFavorit) {
        this.posisiFavorit = posisiFavorit;
    }

    public String getNamaSD() {
        return namaSD;
    }

    public void setNamaSD(String namaSD) {
        this.namaSD = namaSD;
    }

    public String getTahunSD() {
        return tahunSD;
    }

    public void setTahunSD(String tahunSD) {
        this.tahunSD = tahunSD;
    }

    public String getNamaSMP() {
        return namaSMP;
    }

    public void setNamaSMP(String namaSMP) {
        this.namaSMP = namaSMP;
    }

    public String getTahunSMP() {
        return tahunSMP;
    }

    public void setTahunSMP(String tahunSMP) {
        this.tahunSMP = tahunSMP;
    }

    public Object getNamaSMA() {
        return namaSMA;
    }

    public void setNamaSMA(Object namaSMA) {
        this.namaSMA = namaSMA;
    }

    public String getTahunSMA() {
        return tahunSMA;
    }

    public void setTahunSMA(String tahunSMA) {
        this.tahunSMA = tahunSMA;
    }

    public String getNamaUniversitas() {
        return namaUniversitas;
    }

    public void setNamaUniversitas(String namaUniversitas) {
        this.namaUniversitas = namaUniversitas;
    }

    public String getTahunUniversitas() {
        return tahunUniversitas;
    }

    public void setTahunUniversitas(String tahunUniversitas) {
        this.tahunUniversitas = tahunUniversitas;
    }

    public String getTlpSekolahTerakhir() {
        return tlpSekolahTerakhir;
    }

    public void setTlpSekolahTerakhir(String tlpSekolahTerakhir) {
        this.tlpSekolahTerakhir = tlpSekolahTerakhir;
    }

    public String getAnakKe() {
        return anakKe;
    }

    public void setAnakKe(String anakKe) {
        this.anakKe = anakKe;
    }

    public String getJumlahSaudara() {
        return jumlahSaudara;
    }

    public void setJumlahSaudara(String jumlahSaudara) {
        this.jumlahSaudara = jumlahSaudara;
    }

    public String getNamaAyah() {
        return namaAyah;
    }

    public void setNamaAyah(String namaAyah) {
        this.namaAyah = namaAyah;
    }

    public String getNamaIbu() {
        return namaIbu;
    }

    public void setNamaIbu(String namaIbu) {
        this.namaIbu = namaIbu;
    }

    public String getPekerjaanAyah() {
        return pekerjaanAyah;
    }

    public void setPekerjaanAyah(String pekerjaanAyah) {
        this.pekerjaanAyah = pekerjaanAyah;
    }

    public String getPekerjaanIbu() {
        return pekerjaanIbu;
    }

    public void setPekerjaanIbu(String pekerjaanIbu) {
        this.pekerjaanIbu = pekerjaanIbu;
    }

    public String getAirPerGelas() {
        return airPerGelas;
    }

    public void setAirPerGelas(String airPerGelas) {
        this.airPerGelas = airPerGelas;
    }

    public String getMerkAir() {
        return merkAir;
    }

    public void setMerkAir(String merkAir) {
        this.merkAir = merkAir;
    }

    public String getSusuPerHari() {
        return susuPerHari;
    }

    public void setSusuPerHari(String susuPerHari) {
        this.susuPerHari = susuPerHari;
    }

    public String getMerkSusu() {
        return merkSusu;
    }

    public void setMerkSusu(String merkSusu) {
        this.merkSusu = merkSusu;
    }

    public String getVitamin() {
        return vitamin;
    }

    public void setVitamin(String vitamin) {
        this.vitamin = vitamin;
    }

    public Object getMerkVitamin() {
        return merkVitamin;
    }

    public void setMerkVitamin(Object merkVitamin) {
        this.merkVitamin = merkVitamin;
    }

    public String getMakanSayuran() {
        return makanSayuran;
    }

    public void setMakanSayuran(String makanSayuran) {
        this.makanSayuran = makanSayuran;
    }

    public String getMakanBuah() {
        return makanBuah;
    }

    public void setMakanBuah(String makanBuah) {
        this.makanBuah = makanBuah;
    }

    public String getSarapan() {
        return sarapan;
    }

    public void setSarapan(String sarapan) {
        this.sarapan = sarapan;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getBoolVerifikasi() {
        return boolVerifikasi;
    }

    public void setBoolVerifikasi(String boolVerifikasi) {
        this.boolVerifikasi = boolVerifikasi;
    }

    public String getTglVerifikasi() {
        return tglVerifikasi;
    }

    public void setTglVerifikasi(String tglVerifikasi) {
        this.tglVerifikasi = tglVerifikasi;
    }

    public String getTglRegister() {
        return tglRegister;
    }

    public void setTglRegister(String tglRegister) {
        this.tglRegister = tglRegister;
    }

    public String getNamaTeam() {
        return namaTeam;
    }

    public void setNamaTeam(String namaTeam) {
        this.namaTeam = namaTeam;
    }

    public String getThnTeam() {
        return thnTeam;
    }

    public void setThnTeam(String thnTeam) {
        this.thnTeam = thnTeam;
    }

    public String getNoPunggung() {
        return noPunggung;
    }

    public void setNoPunggung(String noPunggung) {
        this.noPunggung = noPunggung;
    }

}

package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dharmana on 6/2/17.
 */

public class Tournament {

    @SerializedName("idTurnamen")
    @Expose
    private String idTurnamen;
    @SerializedName("namaTurnamen")
    @Expose
    private String namaTurnamen;
    @SerializedName("periodeMulai")
    @Expose
    private String periodeMulai;
    @SerializedName("periodeSelesai")
    @Expose
    private String periodeSelesai;
    @SerializedName("dateLastEdit")
    @Expose
    private String dateLastEdit;

    public Tournament(String idTurnamen, String namaTurnamen) {
        setIdTurnamen(idTurnamen);
        setNamaTurnamen(namaTurnamen);
    }

    public String getIdTurnamen() {
        return idTurnamen;
    }

    public void setIdTurnamen(String idTurnamen) {
        this.idTurnamen = idTurnamen;
    }

    public String getNamaTurnamen() {
        return namaTurnamen;
    }

    public void setNamaTurnamen(String namaTurnamen) {
        this.namaTurnamen = namaTurnamen;
    }

    public String getPeriodeMulai() {
        return periodeMulai;
    }

    public void setPeriodeMulai(String periodeMulai) {
        this.periodeMulai = periodeMulai;
    }

    public String getPeriodeSelesai() {
        return periodeSelesai;
    }

    public void setPeriodeSelesai(String periodeSelesai) {
        this.periodeSelesai = periodeSelesai;
    }

    public String getDateLastEdit() {
        return dateLastEdit;
    }

    public void setDateLastEdit(String dateLastEdit) {
        this.dateLastEdit = dateLastEdit;
    }


}

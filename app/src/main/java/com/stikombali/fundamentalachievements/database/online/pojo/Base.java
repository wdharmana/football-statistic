package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by dharmana on 6/2/17.
 */

public class Base {
    @SerializedName("biodataPemain")
    @Expose
    private List<BiodataPemain> biodataPemain = null;
    @SerializedName("tournament")
    @Expose
    private List<Tournament> tournament = null;
    @SerializedName("tournament_detail")
    @Expose
    private List<TournamentDetail> tournamentDetail = null;
    @SerializedName("match")
    @Expose
    private List<Match> match = null;
    @SerializedName("match_detail")
    @Expose
    private List<MatchDetail> matchDetail = null;
    @SerializedName("team")
    @Expose
    private List<Team> team = null;

    @SerializedName("goal_timing")
    @Expose
    private List<GoalTiming> goalTiming = null;

    public List<BiodataPemain> getBiodataPemain() {
        return biodataPemain;
    }

    public void setBiodataPemain(List<BiodataPemain> biodataPemain) {
        this.biodataPemain = biodataPemain;
    }

    public List<Tournament> getTournament() {
        return tournament;
    }

    public void setTournament(List<Tournament> tournament) {
        this.tournament = tournament;
    }

    public List<TournamentDetail> getTournamentDetail() {
        return tournamentDetail;
    }

    public void setTournamentDetail(List<TournamentDetail> tournamentDetail) {
        this.tournamentDetail = tournamentDetail;
    }

    public List<Match> getMatch() {
        return match;
    }

    public void setMatch(List<Match> match) {
        this.match = match;
    }

    public List<MatchDetail> getMatchDetail() {
        return matchDetail;
    }

    public void setMatchDetail(List<MatchDetail> matchDetail) {
        this.matchDetail = matchDetail;
    }

    public List<Team> getTeam() {
        return team;
    }

    public void setTeam(List<Team> team) {
        this.team = team;
    }

    public List<GoalTiming> getGoalTiming() {
        return goalTiming;
    }

    public void setGoalTiming(List<GoalTiming> goalTiming) {
        this.goalTiming = goalTiming;
    }
}

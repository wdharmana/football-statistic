package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dharmana on 6/2/17.
 */

public class TournamentDetail {

    @SerializedName("idTurnamenDetail")
    @Expose
    private String idTurnamenDetail;
    @SerializedName("idTurnamen")
    @Expose
    private String idTurnamen;
    @SerializedName("idTeam")
    @Expose
    private String idTeam;

    public TournamentDetail(String idTurnamenDetail, String idTurnamen, String idTeam) {
        setIdTurnamenDetail(idTurnamenDetail);
        setIdTurnamen(idTurnamen);
        setIdTeam(idTeam);
    }

    public String getIdTurnamenDetail() {
        return idTurnamenDetail;
    }

    public void setIdTurnamenDetail(String idTurnamenDetail) {
        this.idTurnamenDetail = idTurnamenDetail;
    }

    public String getIdTurnamen() {
        return idTurnamen;
    }

    public void setIdTurnamen(String idTurnamen) {
        this.idTurnamen = idTurnamen;
    }

    public String getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(String idTeam) {
        this.idTeam = idTeam;
    }

}

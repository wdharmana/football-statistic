package com.stikombali.fundamentalachievements.database.online.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by dharmana on 6/2/17.
 */

public class Match {

    @SerializedName("idMatch")
    @Expose
    private String idMatch;
    @SerializedName("idTurnamen")
    @Expose
    private String idTurnamen;
    @SerializedName("idPertandingan")
    @Expose
    private String idPertandingan;
    @SerializedName("namaPertandingan")
    @Expose
    private String namaPertandingan;
    @SerializedName("idTeamA")
    @Expose
    private String idTeamA;
    @SerializedName("idTeamB")
    @Expose
    private String idTeamB;
    @SerializedName("GHome")
    @Expose
    private String gHome;
    @SerializedName("GAway")
    @Expose
    private String gAway;
    @SerializedName("periodePertandinganAwal")
    @Expose
    private String periodePertandinganAwal;
    @SerializedName("periodePertandinganAkhir")
    @Expose
    private String periodePertandinganAkhir;
    @SerializedName("periodePost")
    @Expose
    private String periodePost;
    @SerializedName("namaTeamA")
    @Expose
    private String namaTeamA;
    @SerializedName("namaTeamB")
    @Expose
    private String namaTeamB;

    public Match(String idMatch, String idTurnamen, String idTeamA, String idTeamB, String namaPertandingan, String
            namaTeamA, String
                         namaTeamB, String periodePertandinganAwal, String periodePertandinganAkhir) {
        setIdMatch(idMatch);
        setIdTurnamen(idTurnamen);
        setNamaPertandingan(namaPertandingan);
        setIdTeamA(idTeamA);
        setIdTeamB(idTeamB);
        setPeriodePertandinganAwal(periodePertandinganAwal);
        setPeriodePertandinganAkhir(periodePertandinganAkhir);
        setNamaTeamA(namaTeamA);
        setNamaTeamB(namaTeamB);
    }

    public String getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(String idMatch) {
        this.idMatch = idMatch;
    }

    public String getIdTurnamen() {
        return idTurnamen;
    }

    public void setIdTurnamen(String idTurnamen) {
        this.idTurnamen = idTurnamen;
    }

    public String getIdPertandingan() {
        return idPertandingan;
    }

    public void setIdPertandingan(String idPertandingan) {
        this.idPertandingan = idPertandingan;
    }

    public String getNamaPertandingan() {
        return namaPertandingan;
    }

    public void setNamaPertandingan(String namaPertandingan) {
        this.namaPertandingan = namaPertandingan;
    }

    public String getIdTeamA() {
        return idTeamA;
    }

    public void setIdTeamA(String idTeamA) {
        this.idTeamA = idTeamA;
    }

    public String getIdTeamB() {
        return idTeamB;
    }

    public void setIdTeamB(String idTeamB) {
        this.idTeamB = idTeamB;
    }

    public String getGHome() {
        return gHome;
    }

    public void setGHome(String gHome) {
        this.gHome = gHome;
    }

    public String getGAway() {
        return gAway;
    }

    public void setGAway(String gAway) {
        this.gAway = gAway;
    }

    public String getPeriodePertandinganAwal() {
        return periodePertandinganAwal;
    }

    public void setPeriodePertandinganAwal(String periodePertandinganAwal) {
        this.periodePertandinganAwal = periodePertandinganAwal;
    }

    public String getPeriodePertandinganAkhir() {
        return periodePertandinganAkhir;
    }

    public void setPeriodePertandinganAkhir(String periodePertandinganAkhir) {
        this.periodePertandinganAkhir = periodePertandinganAkhir;
    }

    public String getPeriodePost() {
        return periodePost;
    }

    public void setPeriodePost(String periodePost) {
        this.periodePost = periodePost;
    }

    public String getNamaTeamA() {
        return namaTeamA;
    }

    public void setNamaTeamA(String namaTeamA) {
        this.namaTeamA = namaTeamA;
    }

    public String getNamaTeamB() {
        return namaTeamB;
    }

    public void setNamaTeamB(String namaTeamB) {
        this.namaTeamB = namaTeamB;
    }

}

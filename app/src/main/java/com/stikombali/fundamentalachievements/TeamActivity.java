package com.stikombali.fundamentalachievements;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.stikombali.fundamentalachievements.adapter.TeamAdapter;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.DividerItemDecoration;
import com.stikombali.fundamentalachievements.model.Team;

import java.util.ArrayList;
import java.util.List;

public class TeamActivity extends AppCompatActivity implements TeamAdapter.ClickListener {

    RecyclerView rv;
    DBConfig dbConfig;
    TeamAdapter teamAdapter;
    String tourName;
    String v_tournament_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setSubtitle(getIntent().getStringExtra("v_tournament_name"));

        v_tournament_id = getIntent().getStringExtra("v_tournament_id");

        rv = (RecyclerView) findViewById(R.id.rv);
        dbConfig = new DBConfig(this);
        teamAdapter = new TeamAdapter(this);
        getData();
        setMainRecyclerView();

    }

    public void getData() {
        List<Team> data = new ArrayList<>();
        Team mainInfo = null;
        Cursor c = dbConfig.getTeamData(v_tournament_id);
        if (c != null) {
            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex("name");
                String nameText = c.getString(nameIndex);
                String idText = c.getString(c.getColumnIndex("id"));

                Team team = new Team();
                team.setName(nameText);
                team.setId(idText);
                teamAdapter.addItem(team);

            }
        }

    }

    public void setMainRecyclerView() {

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(teamAdapter);
    }

    @Override
    public void onClick(int position) {
        Team selectedItem = teamAdapter.getSelectedItem(position);
        Intent myintent = new Intent(TeamActivity.this, PlayerActivity.class);
        myintent.putExtra("v_team_id", selectedItem.getId());
        myintent.putExtra("v_team_name", selectedItem.getName());
        myintent.putExtra("action", "");
        startActivity(myintent);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

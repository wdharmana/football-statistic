package com.stikombali.fundamentalachievements;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class PlayerMatchActivity extends AppCompatActivity {
    Integer NUM_ROW = 20;
    String tr = "", th;
    String match_id, match_name, team_a, team_b, action, action_name;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_match);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        team_a = getIntent().getStringExtra("match_team_a");
        team_b = getIntent().getStringExtra("match_team_b");

        action = getIntent().getStringExtra("action");
        match_id = getIntent().getStringExtra("match_id");
        match_name = getIntent().getStringExtra("match_name");
        action_name = getIntent().getStringExtra("action_name");
        getSupportActionBar().setTitle("Match Player");
        getSupportActionBar().setSubtitle(match_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        init();

    }

    private void init() {
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        init();
    }

    private void setupViewPager(ViewPager viewPager) {

        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        Bundle bundleA = new Bundle();
        bundleA.putString("team", team_a);
        bundleA.putString("pos", "left");
        bundleA.putString("match_id", match_id);
        FragmentPlayer teamA = new FragmentPlayer();
        teamA.setArguments(bundleA);
        adapter.addFragment(teamA, "TEAM A");

        FragmentPlayer teamB = new FragmentPlayer();
        teamB.setArguments(bundleA);

        Bundle bundle = new Bundle();
        bundle.putString("team", team_b);
        bundle.putString("pos", "right");
        bundle.putString("match_id", match_id);
        FragmentPlayer fragB = new FragmentPlayer();
        fragB.setArguments(bundle);


        adapter.addFragment(fragB, "TEAM B");
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}

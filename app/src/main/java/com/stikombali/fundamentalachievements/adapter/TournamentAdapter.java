package com.stikombali.fundamentalachievements.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stikombali.fundamentalachievements.R;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.ItemClickListener;
import com.stikombali.fundamentalachievements.model.Tournament;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dharmana on 4/9/17.
 */

public class TournamentAdapter extends RecyclerView.Adapter<TournamentAdapter.TournamentHolder> {


    DBConfig myDatabase;
    private List<Tournament> mainInfo;
    private ClickListener mListener;

    /*
        public TournamentAdapter(List<Tournament> mainInfo) {
            this.mainInfo = mainInfo;
        }
    */
    public TournamentAdapter(ClickListener listener) {
        this.mainInfo = new ArrayList<>();
        this.mListener = listener;
    }

    public void addItem(Tournament item) {
        mainInfo.add(item);
        notifyDataSetChanged();
    }

    public Tournament getSelectedItem(int position) {
        return mainInfo.get(position);
    }

    @Override
    public TournamentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tournament_raw, parent, false);
        TournamentHolder holder = new TournamentHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final TournamentHolder holder, final int position) {
        holder.txtName.setText(mainInfo.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mainInfo.size();
    }


    public interface ClickListener {
        void onClick(int position);
    }

    public class TournamentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName;
        private ItemClickListener clickListener;

        public TournamentHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(getLayoutPosition());
        }
    }

}

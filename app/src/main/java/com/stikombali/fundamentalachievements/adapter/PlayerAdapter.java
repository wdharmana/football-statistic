package com.stikombali.fundamentalachievements.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stikombali.fundamentalachievements.R;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.ItemClickListener;
import com.stikombali.fundamentalachievements.model.Player;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dharmana on 4/9/17.
 */

public class PlayerAdapter extends RecyclerView.Adapter<PlayerAdapter.PlayerHolder> {


    DBConfig myDatabase;
    private List<Player> mainInfo;
    private ClickListener mListener;

    public PlayerAdapter(ClickListener listener) {
        this.mainInfo = new ArrayList<>();
        this.mListener = listener;
    }

    public void addItem(Player item) {
        mainInfo.add(item);
        notifyDataSetChanged();
    }

    public Player getSelectedItem(int position) {
        return mainInfo.get(position);
    }

    @Override
    public PlayerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.player_raw, parent, false);
        PlayerHolder holder = new PlayerHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final PlayerHolder holder, final int position) {
        holder.txtName.setText(mainInfo.get(position).getName());
        holder.txtPlayerNumber.setText(mainInfo.get(position).getPlayer_number());
        holder.txtAccountNumber.setText(mainInfo.get(position).getAccount_number());
    }

    @Override
    public int getItemCount() {
        return mainInfo.size();
    }


    public interface ClickListener {
        void onClick(int position);
    }

    public class PlayerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName, txtPlayerNumber, txtAccountNumber;
        private ItemClickListener clickListener;

        public PlayerHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtPlayerNumber = (TextView) itemView.findViewById(R.id.txtPlayerNumber);
            txtAccountNumber = (TextView) itemView.findViewById(R.id.txtAccountNumber);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(getLayoutPosition());
        }
    }

}

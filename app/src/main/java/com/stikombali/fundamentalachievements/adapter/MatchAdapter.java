package com.stikombali.fundamentalachievements.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stikombali.fundamentalachievements.R;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.ItemClickListener;
import com.stikombali.fundamentalachievements.model.Match;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dharmana on 4/9/17.
 */

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MatchHolder> {


    DBConfig myDatabase;
    private List<Match> mainInfo;
    private ClickListener mListener;

    public MatchAdapter(ClickListener listener) {
        this.mainInfo = new ArrayList<>();
        this.mListener = listener;
    }

    public void addItem(Match item) {
        mainInfo.add(item);
        notifyDataSetChanged();
    }

    public Match getSelectedItem(int position) {
        return mainInfo.get(position);
    }

    @Override
    public MatchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.match_raw, parent, false);
        MatchHolder holder = new MatchHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MatchHolder holder, final int position) {
        holder.txtName.setText(mainInfo.get(position).getNama_team_a() + " VS " + mainInfo.get(position).getNama_team_b
                () + " (" + mainInfo.get(position).getName() + ")");
        holder.txtStart.setText(mainInfo.get(position).getPeriode_start());
    }

    @Override
    public int getItemCount() {
        return mainInfo.size();
    }


    public interface ClickListener {
        void onClick(int position);
    }

    public class MatchHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName, txtStart;
        private ItemClickListener clickListener;

        public MatchHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            txtStart = (TextView) itemView.findViewById(R.id.txtPeriodeStart);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(getLayoutPosition());
        }
    }

}

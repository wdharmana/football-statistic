package com.stikombali.fundamentalachievements.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.stikombali.fundamentalachievements.R;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.ItemClickListener;
import com.stikombali.fundamentalachievements.model.Team;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dharmana on 4/9/17.
 */

public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.TeamHolder> {


    DBConfig myDatabase;
    private List<Team> mainInfo;
    private ClickListener mListener;

    public TeamAdapter(ClickListener listener) {
        this.mainInfo = new ArrayList<>();
        this.mListener = listener;
    }

    public void addItem(Team item) {
        mainInfo.add(item);
        notifyDataSetChanged();
    }

    public Team getSelectedItem(int position) {
        return mainInfo.get(position);
    }

    @Override
    public TeamHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tournament_raw, parent, false);
        TeamHolder holder = new TeamHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(final TeamHolder holder, final int position) {
        holder.txtName.setText(mainInfo.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return mainInfo.size();
    }


    public interface ClickListener {
        void onClick(int position);
    }

    public class TeamHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txtName;
        private ItemClickListener clickListener;

        public TeamHolder(View itemView) {
            super(itemView);
            txtName = (TextView) itemView.findViewById(R.id.txtName);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mListener.onClick(getLayoutPosition());
        }
    }

}

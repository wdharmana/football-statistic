package com.stikombali.fundamentalachievements;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.stikombali.fundamentalachievements.adapter.TournamentAdapter;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.helper.DividerItemDecoration;
import com.stikombali.fundamentalachievements.model.Tournament;

import java.util.ArrayList;
import java.util.List;

public class TurnamenActivity extends AppCompatActivity implements TournamentAdapter.ClickListener {

    RecyclerView rv;
    DBConfig dbConfig;
    TournamentAdapter tournamentAdapter;
    String tourName;
    String action;
    Intent myintent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_turnamen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        action = getIntent().getStringExtra("action");


        rv = (RecyclerView) findViewById(R.id.rv);
        dbConfig = new DBConfig(this);
        tournamentAdapter = new TournamentAdapter(this);
        getData();
        setMainRecyclerView();

    }

    public void getData() {
        List<Tournament> data = new ArrayList<>();
        Tournament mainInfo = null;
        Cursor c = dbConfig.getTournamenData();
        if (c != null) {
            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex(DBConfig.TOURNAMEN_COL_NAME);
                String nameText = c.getString(nameIndex);
                int idIndex = c.getColumnIndex(DBConfig.TOURNAMEN_COL_ID);
                String idText = c.getString(idIndex);

                this.tourName = nameText;
                Tournament tournament = new Tournament();
                tournament.setName(nameText);
                tournament.setId(idText);
                tournamentAdapter.addItem(tournament);

            }
        }

    }

    public void setMainRecyclerView() {

        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(tournamentAdapter);
    }

    @Override
    public void onClick(int position) {
        Tournament selectedItem = tournamentAdapter.getSelectedItem(position);
        if (action.equals("team")) {
            myintent = new Intent(TurnamenActivity.this, TeamActivity.class);
            myintent.putExtra("action", "");
        } else if (action.equals("match")) {
            myintent = new Intent(TurnamenActivity.this, MatchListActivity.class);
            myintent.putExtra("action", "-");
        } else if (action.equals("touch")) {
            myintent = new Intent(TurnamenActivity.this, MatchListActivity.class);
            myintent.putExtra("action", action);
        } else if (action.equals("fouls_passing")) {
            myintent = new Intent(TurnamenActivity.this, MatchListActivity.class);
            myintent.putExtra("action", action);
        } else if (action.equals("fouls_control")) {
            myintent = new Intent(TurnamenActivity.this, MatchListActivity.class);
            myintent.putExtra("action", action);
        } else if (action.equals("sgc")) {
            myintent = new Intent(TurnamenActivity.this, MatchListActivity.class);
            myintent.putExtra("action", action);
        } else if (action.equals("keeper")) {
            myintent = new Intent(TurnamenActivity.this, MatchListActivity.class);
            myintent.putExtra("action", action);
        }

        myintent.putExtra("v_tournament_id", selectedItem.getId());
        myintent.putExtra("v_tournament_name", selectedItem.getName());
        startActivity(myintent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package com.stikombali.fundamentalachievements;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.stikombali.fundamentalachievements.adapter.CadanganAdapter;
import com.stikombali.fundamentalachievements.adapter.CadanganAdapter2;
import com.stikombali.fundamentalachievements.database.DBConfig;
import com.stikombali.fundamentalachievements.database.DataExport;
import com.stikombali.fundamentalachievements.database.online.RestManager;
import com.stikombali.fundamentalachievements.helper.CSVWriter;
import com.stikombali.fundamentalachievements.helper.Utils;
import com.stikombali.fundamentalachievements.model.Player;

import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.stikombali.fundamentalachievements.helper.Utils.verifyStoragePermissions;

public class MatchActivity extends AppCompatActivity implements CadanganAdapter.ClickListener, CadanganAdapter2.ClickListener {
    public LinearLayout llField;

    public Button btnLeft11, btnLeft21, btnLeft22, btnLeft23, btnLeft24, btnLeft31, btnLeft32, btnLeft33, btnLeft34,
            btnLeft41, btnLeft42, btnLeft43, btnLeft44;
    public Button btnRight11, btnRight21, btnRight22, btnRight23, btnRight24, btnRight31, btnRight32, btnRight33, btnRight34,
            btnRight41, btnRight42, btnRight43, btnRight44;

    public TextView txtLeft11, txtLeft21, txtLeft22, txtLeft23, txtLeft24, txtLeft31, txtLeft32, txtLeft33, txtLeft34,
            txtLeft41, txtLeft42, txtLeft43, txtLeft44, txtRight11, txtRight21,
            txtRight22, txtRight23, txtRight24, txtRight31, txtRight32, txtRight33, txtRight34, txtRight41, txtRight42, txtRight43, txtRight44;

    public CoordinatorLayout coordinatorLayout;
    public String[] listPosition = {"11", "21", "22", "23", "24", "31", "32", "33", "34", "41", "42", "43", "44"};
    public Button[] listButton = {btnLeft11, btnLeft21, btnLeft22, btnLeft23, btnLeft24, btnLeft31, btnLeft32, btnLeft33,
            btnLeft34, btnLeft41, btnLeft42, btnLeft43, btnLeft44, btnRight11, btnRight21, btnRight22, btnRight23, btnRight24,
            btnRight31, btnRight32, btnRight33, btnRight34,
            btnRight41, btnRight42, btnRight43, btnRight44};
    public TextView[] listTextView = {txtLeft11, txtLeft21, txtLeft22, txtLeft23, txtLeft24, txtLeft31, txtLeft32,
            txtLeft33, txtLeft34,
            txtLeft41, txtLeft42, txtLeft43, txtLeft44, txtRight11, txtRight21,
            txtRight22, txtRight23, txtRight24, txtRight31, txtRight32, txtRight33, txtRight34, txtRight41,
            txtRight42, txtRight43, txtRight44};
    String team_a, team_b, match_id, action_name, action, match_name;
    Button selButton;
    TextView selText;
    CadanganAdapter cadanganAdapter1;
    CadanganAdapter2 cadanganAdapter2;
    LinearLayout content_match, llCadangan;
    int boolMode = 0;
    boolean boolCadangan = true;
    List<String> kiriList = new ArrayList<String>();
    List<String> kananList = new ArrayList<String>();

    DBConfig dbConfig;

    RecyclerView rv1, rv2;

    Boolean editMode = false;
    String curCadanganNo = null, curCadanganNickname = "", curCadanganTeam = "";
    Toolbar toolbar;

    double hCadangan;

    DataExport jsonExport;

    RestManager mManager;

    ProgressDialog mDialog;

    public static String joinList(List list, String literal) {
        return list.toString().replaceAll(",", literal).replaceAll("[\\[.\\].\\s+]", "");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        action_name = getIntent().getStringExtra("action_name");
        action = getIntent().getStringExtra("action");
        match_name = getIntent().getStringExtra("match_name");

        getSupportActionBar().setTitle(match_name);
        getSupportActionBar().setSubtitle(action_name);

        mManager = new RestManager();

        dbConfig = new DBConfig(this);
        jsonExport = new DataExport();
        match_id = getIntent().getStringExtra("match_id");

        llField = (LinearLayout) findViewById(R.id.soccer_field);
        content_match = (LinearLayout) findViewById(R.id.content_match);
        llCadangan = (LinearLayout) findViewById(R.id.llCadangan);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);

        txtLeft11 = (TextView) findViewById(R.id.ltext_1_1);
        txtLeft21 = (TextView) findViewById(R.id.ltext_2_1);
        txtLeft22 = (TextView) findViewById(R.id.ltext_2_2);
        txtLeft23 = (TextView) findViewById(R.id.ltext_2_3);
        txtLeft24 = (TextView) findViewById(R.id.ltext_2_4);

        txtLeft31 = (TextView) findViewById(R.id.ltext_3_1);
        txtLeft32 = (TextView) findViewById(R.id.ltext_3_2);
        txtLeft33 = (TextView) findViewById(R.id.ltext_3_3);
        txtLeft34 = (TextView) findViewById(R.id.ltext_3_4);

        txtLeft41 = (TextView) findViewById(R.id.ltext_4_1);
        txtLeft42 = (TextView) findViewById(R.id.ltext_4_2);
        txtLeft43 = (TextView) findViewById(R.id.ltext_4_3);
        txtLeft44 = (TextView) findViewById(R.id.ltext_4_4);

        txtRight11 = (TextView) findViewById(R.id.rtext_1_1);
        txtRight21 = (TextView) findViewById(R.id.rtext_2_1);
        txtRight22 = (TextView) findViewById(R.id.rtext_2_2);
        txtRight23 = (TextView) findViewById(R.id.rtext_2_3);
        txtRight24 = (TextView) findViewById(R.id.rtext_2_4);

        txtRight31 = (TextView) findViewById(R.id.rtext_3_1);
        txtRight32 = (TextView) findViewById(R.id.rtext_3_2);
        txtRight33 = (TextView) findViewById(R.id.rtext_3_3);
        txtRight34 = (TextView) findViewById(R.id.rtext_3_4);

        txtRight41 = (TextView) findViewById(R.id.rtext_4_1);
        txtRight42 = (TextView) findViewById(R.id.rtext_4_2);
        txtRight43 = (TextView) findViewById(R.id.rtext_4_3);
        txtRight44 = (TextView) findViewById(R.id.rtext_4_4);


        team_a = getIntent().getStringExtra("match_team_a");
        team_b = getIntent().getStringExtra("match_team_b");

        // Log.e("log",team_a);


        setupFieldMode(boolMode);
        setupButton();

    }

    public void setupFieldMode(int mode) {
        int width = 0;
        width = getWindowManager().getDefaultDisplay().getWidth();
        if (mode == 0) {
            double height = width / 1.7;
            llField.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
        } else if (mode == 1) {
            int hc = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 62, getResources()
                    .getDisplayMetrics());
            double height = content_match.getHeight() - hc;
            llField.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
        }
    }

    public void showCadanganBar(boolean s) {
        if (s) {
            llCadangan.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            if (boolMode == 1) {
                double height = content_match.getHeight() - hCadangan;
                llField.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
            } else {
                int width = getWindowManager().getDefaultDisplay().getWidth();
                double height = width / 1.7;
                llField.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
            }
        } else {
            llCadangan.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    0));
            if (boolMode == 1) {
                double height = content_match.getHeight();
                llField.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
            } else {
                int width = getWindowManager().getDefaultDisplay().getWidth();
                double height = width / 1.7;
                llField.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (int) height));
            }
        }
        boolCadangan = !boolCadangan;

    }

    public void runSnack(String msg) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    public void executeLong(String team, Button button, TextView txt) {
        String btext = button.getText().toString();
        Log.e("LOG-TEXT", btext);
        if (!btext.equals("")) {
            openPlayerOpt(team, button, txt);
        } else {
            openPlayer("new", team, button, txt);
        }
    }

    public void executeClick(Button b, String team) {
        String btext = b.getText().toString();

        if (!btext.equals("")) {
            if (!editMode) {
                runAction(team, btext);
            } else {
                if (curCadanganTeam.equals(team)) {
                    int idxButton = getIndexButton(b);
                    TextView tv = getTextView(idxButton);

                    openCadangan(team, b, tv);
                    runSnack("Berhasil dipilih");
                    fieldMode(true);
                    loadCadangan();
                } else {
                    runSnack("Pemain ini tidak bisa dipilih. Pilih pemain dari tim asal.");
                }
            }

        }
    }

    public void setupButton() {
        btnLeft11 = (Button) findViewById(R.id.left_1_1);
        btnLeft21 = (Button) findViewById(R.id.left_2_1);
        btnLeft22 = (Button) findViewById(R.id.left_2_2);
        btnLeft23 = (Button) findViewById(R.id.left_2_3);
        btnLeft24 = (Button) findViewById(R.id.left_2_4);
        btnLeft31 = (Button) findViewById(R.id.left_3_1);
        btnLeft32 = (Button) findViewById(R.id.left_3_2);
        btnLeft33 = (Button) findViewById(R.id.left_3_3);
        btnLeft34 = (Button) findViewById(R.id.left_3_4);
        btnLeft41 = (Button) findViewById(R.id.left_4_1);
        btnLeft42 = (Button) findViewById(R.id.left_4_2);
        btnLeft43 = (Button) findViewById(R.id.left_4_3);
        btnLeft44 = (Button) findViewById(R.id.left_4_4);

        btnRight11 = (Button) findViewById(R.id.right_1_1);
        btnRight21 = (Button) findViewById(R.id.right_2_1);
        btnRight22 = (Button) findViewById(R.id.right_2_2);
        btnRight23 = (Button) findViewById(R.id.right_2_3);
        btnRight24 = (Button) findViewById(R.id.right_2_4);
        btnRight31 = (Button) findViewById(R.id.right_3_1);
        btnRight32 = (Button) findViewById(R.id.right_3_2);
        btnRight33 = (Button) findViewById(R.id.right_3_3);
        btnRight34 = (Button) findViewById(R.id.right_3_4);
        btnRight41 = (Button) findViewById(R.id.right_4_1);
        btnRight42 = (Button) findViewById(R.id.right_4_2);
        btnRight43 = (Button) findViewById(R.id.right_4_3);
        btnRight44 = (Button) findViewById(R.id.right_4_4);


        /*      setup onclick       */
        btnLeft11.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft11, txtLeft11);
                return false;
            }
        });

        btnLeft11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                if (action.equals("keeper")) {
                    executeClick(b, team_a);
                } else {
                    if (editMode) {
                        executeClick(b, team_a);
                    } else {
                        runSnack("Tidak ada penilaian untuk posisi ini.");
                    }
                }
            }
        });

        /*  START */
        btnLeft21.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Button b = (Button) v;
                executeLong(team_a, btnLeft21, txtLeft21);
                return false;

            }
        });
        btnLeft21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });
        /* END */

        btnLeft22.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft22, txtLeft22);
                return false;
            }
        });
        btnLeft22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });
        /* END */

        btnLeft23.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft23, txtLeft23);
                return false;
            }
        });
        btnLeft23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft24.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft24, txtLeft24);
                return false;
            }
        });
        btnLeft24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });


        btnLeft31.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft31, txtLeft31);
                return false;
            }
        });
        btnLeft31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft32.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft32, txtLeft32);
                return false;
            }
        });
        btnLeft32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft33.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft33, txtLeft33);
                return false;
            }
        });
        btnLeft33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft34.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft34, txtLeft34);
                return false;
            }
        });
        btnLeft34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft41.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft41, txtLeft41);
                return false;
            }
        });
        btnLeft41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });


        btnLeft42.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft42, txtLeft42);
                return false;
            }
        });
        btnLeft42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft43.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft43, txtLeft43);
                return false;
            }
        });
        btnLeft43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });

        btnLeft44.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_a, btnLeft44, txtLeft44);
                return false;
            }
        });
        btnLeft44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_a);
            }
        });


        /*      setup onclick       */
        btnRight11.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight11, txtRight11);
                return false;
            }
        });

        btnRight11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                if (action.equals("keeper")) {
                    executeClick(b, team_b);
                } else {
                    if (editMode) {
                        executeClick(b, team_b);
                    } else {
                        runSnack("Tidak ada penilaian untuk posisi ini.");
                    }

                }
            }
        });

        /*  START */
        btnRight21.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Button b = (Button) v;
                executeLong(team_b, btnRight21, txtRight21);
                return false;

            }
        });
        btnRight21.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });
        /* END */

        btnRight22.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight22, txtRight22);
                return false;
            }
        });
        btnRight22.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });
        /* END */

        btnRight23.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight23, txtRight23);
                return false;
            }
        });
        btnRight23.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        btnRight24.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight24, txtRight24);
                return false;
            }
        });
        btnRight24.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });


        btnRight31.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight31, txtRight31);
                return false;
            }
        });
        btnRight31.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        btnRight32.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight32, txtRight32);
                return false;
            }
        });
        btnRight32.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        btnRight33.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight33, txtRight33);
                return false;
            }
        });
        btnRight33.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        btnRight34.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight34, txtRight34);
                return false;
            }
        });
        btnRight34.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });


        btnRight41.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight41, txtRight41);
                return false;
            }
        });
        btnRight41.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });


        btnRight42.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight42, txtRight42);
                return false;
            }
        });
        btnRight42.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        btnRight43.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight43, txtRight43);
                return false;
            }
        });
        btnRight43.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        btnRight44.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                executeLong(team_b, btnRight44, txtRight44);
                return false;
            }
        });
        btnRight44.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button b = (Button) v;
                executeClick(b, team_b);
            }
        });

        boolean matchStatus = dbConfig.checkMatchStatus(match_id);
        /* Fetch Formation Here */

        if (matchStatus) {
            fetchFormation(team_a);
            fetchFormation(team_b);
        }


        setupRvCadangan();
    }

    public void setupRvCadangan() {
        rv1 = (RecyclerView) findViewById(R.id.rvTeamA);
        rv2 = (RecyclerView) findViewById(R.id.rvTeamB);
        cadanganAdapter1 = new CadanganAdapter(this);
        cadanganAdapter2 = new CadanganAdapter2(this);
        getCadangan(team_a, cadanganAdapter1, null);
        getCadangan(team_b, null, cadanganAdapter2);

        rv1.setHasFixedSize(true);
        rv1.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv1.setAdapter(cadanganAdapter1);

        rv2.setHasFixedSize(true);
        rv2.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, true));
        rv2.setAdapter(cadanganAdapter2);

    }

    public void loadCadangan() {
        getCadangan(team_a, cadanganAdapter1, null);
        getCadangan(team_b, null, cadanganAdapter2);
    }

    public void getCadangan(String team, CadanganAdapter pa, CadanganAdapter2 pa2) {
        List<Player> data = new ArrayList<>();
        Player mainInfo = null;

        Cursor c = dbConfig.getPlayerCadangan(team, match_id);
        if (c != null) {

            if (pa == null) {
                pa2.clearItem();
            } else {
                pa.clearItem();
            }

            while (c.moveToNext()) {
                int nameIndex = c.getColumnIndex("name");
                String nameText = c.getString(nameIndex);
                String idText = c.getString(c.getColumnIndex("id"));
                String playerNumberText = c.getString(c.getColumnIndex("player_number"));
                String accountNumberText = c.getString(c.getColumnIndex("account_number"));
                String nicknameText = c.getString(c.getColumnIndex("nickname"));
                String statusText = c.getString(c.getColumnIndex("status"));

                Player player = new Player();
                player.setName(nameText);
                player.setId(idText);
                player.setPlayer_number(playerNumberText);
                player.setAccount_number(accountNumberText);
                player.setNickname(nicknameText);
                player.setStatus(statusText);
                if (pa == null) {
                    pa2.addItem(player);
                } else {
                    pa.addItem(player);
                }


            }
        }

    }

    public void removePlayer(String team_id, Button btn, TextView txt) {
        List<String> list;
        if (checkTeam(team_id).equals("left")) {
            list = kiriList;
        } else {
            list = kananList;
        }
        String player = btn.getText().toString();
        dbConfig.removeFormation(match_id, team_id, player);
        removeItem(list, player);

        btn.setText("");
        txt.setText("");
        btn.setBackgroundResource(R.drawable.touch_transparent);
        loadCadangan();
    }

    public void runAction(String team, String playernumber) {
        String pid = dbConfig.getPlayerId(team, match_id, playernumber);
        Log.e("PID", pid);
        if (action.equals("sgc")) {

            optSgc(team, playernumber);
        } else if (action.equals("keeper")) {
            optKeeper(team, playernumber);
        } else {
            runSnack("+1 " + action_name + " untuk pemain nomor " + playernumber);
            Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
            // Vibrate for 500 milliseconds
            vib.vibrate(500);
            executeDetail(action, team, playernumber);
        }
    }

    public void optSgc(String team, String playernumber) {
        final String data_team = team;
        final String data_player = playernumber;

        Cursor pd = dbConfig.getPlayerDetail(team, match_id, playernumber);

        List<String> mOpt = new ArrayList<String>();
        mOpt.add("Shoot on Target");
        mOpt.add("Goal");
        mOpt.add("Pelanggaran");
        mOpt.add("Kartu Kuning");
        mOpt.add("Kartu Merah");
        //Create sequence of items
        final CharSequence[] Opts = mOpt.toArray(new String[mOpt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(pd.getString(pd.getColumnIndex("nickname")) + " (" + playernumber + ")");
        dialogBuilder.setItems(Opts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String selectedText = Opts[item].toString();  //Selected item in listview
                String exe_act = "";
                if (selectedText.equals("Shoot on Target")) {
                    exe_act = "sot";
                } else if (selectedText.equals("Goal")) {
                    exe_act = "goal";
                } else if (selectedText.equals("Pelanggaran")) {
                    exe_act = "fouls";
                } else if (selectedText.equals("Kartu Kuning")) {
                    exe_act = "yellow_card";
                } else if (selectedText.equals("Kartu Merah")) {
                    exe_act = "red_card";
                }

                if (exe_act.equals("goal")) {
                    String postDt = null;
                    try {
                        // Create an instance of SimpleDateFormat used for formatting
// the string representation of date (month/day/year)
                        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);

// Get the date today using Calendar object.
                        Date today = Calendar.getInstance().getTime();
                        postDt = df.format(today);
                        Log.e("MY DATETIME", postDt);

                    } catch (Exception e) {
                        Log.e("err", e.getMessage());
                    }

                    Intent goalIntent = new Intent(getApplicationContext(), GoalTimeActivity.class);
                    goalIntent.putExtra("match_start", getIntent().getStringExtra("match_start"));
                    goalIntent.putExtra("team_id", data_team);
                    goalIntent.putExtra("player", data_player);
                    goalIntent.putExtra("post_time", postDt);
                    startActivityForResult(goalIntent, 333);


                } else {

                    runSnack("+1 " + selectedText + " untuk pemain nomor " + data_player);
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    vib.vibrate(500);
                    if (!selectedText.equals("Kartu Kuning") && !selectedText.equals("Kartu Merah")) {
                        executeDetail(exe_act, data_team, data_player);
                    } else {
                        executeDetail(exe_act, data_team, data_player);
                        executeDetail("fouls", data_team, data_player);
                    }

                }
            }
        });
        dialogBuilder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }

        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    public void optKeeper(String team, String playernumber) {
        final String data_team = team;
        final String data_player = playernumber;

        Cursor pd = dbConfig.getPlayerDetail(team, match_id, playernumber);

        List<String> mOpt = new ArrayList<String>();
        // mOpt.add("Reaction");
        mOpt.add("Saving");
        mOpt.add("Distribution");
        mOpt.add("Goal");
        //Create sequence of items
        final CharSequence[] Opts = mOpt.toArray(new String[mOpt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle(pd.getString(pd.getColumnIndex("nickname")) + " (" + playernumber + ")");
        dialogBuilder.setItems(Opts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String selectedText = Opts[item].toString();  //Selected item in listview
                String exe_act = "";

                if (!selectedText.equals("Reaction")) {
                    if (selectedText.equals("Saving")) {
                        exe_act = "saving";
                    } else if (selectedText.equals("Distribution")) {
                        exe_act = "distribution";
                    } else if (selectedText.equals("Goal")) {
                        exe_act = "anticipation";
                    }

                    runSnack("+1 " + selectedText + " untuk pemain nomor " + data_player);
                    Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                    // Vibrate for 500 milliseconds
                    vib.vibrate(500);
                    executeDetail(exe_act, data_team, data_player);

                } else {
                    openReaction(data_team, data_player);
                }

            }
        });
        dialogBuilder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }

        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void openReaction(final String data_team, final String data_player) {


        List<String> mOpt = new ArrayList<String>();
        mOpt.add("Good");
        mOpt.add("Enough");
        mOpt.add("Less");
        //Create sequence of items
        final CharSequence[] Opts = mOpt.toArray(new String[mOpt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Reaction");
        dialogBuilder.setItems(Opts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String selectedText = Opts[item].toString();  //Selected item in listview
                String exe_act = "";

                exe_act = selectedText;


                runSnack("Reaction " + selectedText + " untuk pemain nomor " + data_player);
                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);
                String pid = dbConfig.getPlayerId(data_team, match_id, data_player);
                dbConfig.setMatchDetailReaction(selectedText, match_id, pid);


            }
        });
        dialogBuilder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }

        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();

    }

    private void openSotCSV() {
        List<String> mOpt = new ArrayList<String>();
        mOpt.add("Shoot, Goal & Kartu");
        mOpt.add("Goal Timing");
        //Create sequence of items
        final CharSequence[] Opts = mOpt.toArray(new String[mOpt.size()]);
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Export CSV");
        dialogBuilder.setItems(Opts, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                String selectedText = Opts[item].toString();  //Selected item in listview
                if (selectedText.equals("Goal Timing")) {
                    exportCSV("goaltiming");
                } else {
                    exportCSV(null);
                }

            }
        });
        dialogBuilder.setPositiveButton("Batal", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }

        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();

    }

    public void executeDetail(String action, String team, String playernumber) {
        String pid = dbConfig.getPlayerId(team, match_id, playernumber);
        dbConfig.setMatchDetailData(action, match_id, pid);
    }

    public void openPlayer(String mode, String team_id, Button btn, TextView txt) {

        selButton = btn;
        selText = txt;
        String pos = checkTeam(team_id);

        int idxButton = getIndexButton(btn);
        String curpos;

        int listLen = listPosition.length;

        if (idxButton >= listLen) {
            curpos = listPosition[idxButton - (listLen)];
        } else {
            curpos = listPosition[idxButton];
        }

        Intent playerIntent = new Intent(this, PlayerActivity.class);
        playerIntent.putExtra("v_team_id", team_id);
        playerIntent.putExtra("v_match_id", match_id);
        playerIntent.putExtra("v_position", curpos);
        playerIntent.putExtra("action", "pick_player");

        String arrJoined;

        if (pos.equals("left")) {
            arrJoined = joinList(kiriList, ",");
        } else {
            arrJoined = joinList(kananList, ",");
        }

        playerIntent.putExtra("mode", mode);
        playerIntent.putExtra("cur_player", arrJoined);
        playerIntent.putExtra("player_old", btn.getText().toString());

        startActivityForResult(playerIntent, 222);
    }

    public void openCadangan(String team_id, Button btn, TextView textView) {
        String playerOld = btn.getText().toString();
        selButton = btn;
        selText = textView;
        List<String> list;

        if (checkTeam(team_id).equals("left")) {
            list = kiriList;
            selButton.setBackgroundResource(R.drawable.touch_green);
        } else {
            list = kananList;
            selButton.setBackgroundResource(R.drawable.touch_blue);
        }


        removeItem(list, playerOld);

        String cadanganId = dbConfig.getPlayerId(team_id, match_id, curCadanganNo);
        dbConfig.updateFormation(match_id, team_id, playerOld, cadanganId, getPositionName(selButton));

        selButton.setText(curCadanganNo);
        selText.setText(curCadanganNickname);

    }

    public void openPlayerOpt(final String team_id, final Button btn, final TextView txt) {
        String valText = "";
        String pid = dbConfig.getPlayerId(team_id, match_id, btn.getText().toString());
        if (!action.equals("sgc") && !action.equals("keeper")) {

            int mydata = dbConfig.getMDVal(action, match_id, pid);
            String valData = String.valueOf(mydata);
            valText = "\n" + action_name + ": " + valData;

        } else if (action.equals("keeper")) {
            String savingData = String.valueOf(dbConfig.getMDVal("saving", match_id, pid));
            String distData = String.valueOf(dbConfig.getMDVal("distribution", match_id, pid));
            String antiData = String.valueOf(dbConfig.getMDVal("anticipation", match_id, pid));
            valText = "\n\nSaving: " + savingData;
            valText = valText + "\nDistribution: " + distData;
            valText = valText + "\nGoal:" + antiData;
        } else {
            String sotData = String.valueOf(dbConfig.getMDVal("sot", match_id, pid));
            String goalData = String.valueOf(dbConfig.getMDVal("goal", match_id, pid));
            String foulData = String.valueOf(dbConfig.getMDVal("fouls", match_id, pid));
            String ycData = String.valueOf(dbConfig.getMDVal("yellow_card", match_id, pid));
            String rcData = String.valueOf(dbConfig.getMDVal("red_card", match_id, pid));
            valText = "\n\nShoot on Target: " + sotData;
            valText = valText + "\nPelanggaran: " + foulData;
            valText = valText + "\nGoal: " + goalData;
            valText = valText + "\nKartu: Kuning(" + ycData + ") Merah(" + rcData + ")";
        }

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MatchActivity.this);
        alertDialogBuilder.setTitle("Informasi Pemain");
        alertDialogBuilder.setMessage("Nama Pemain: " + txt.getText().toString() + "\nNomor Punggung: " + btn.getText()
                .toString() + valText);
        alertDialogBuilder.setPositiveButton("Ganti", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                openPlayer("update", team_id, btn, txt);
            }

        });
        alertDialogBuilder.setNegativeButton("Hapus", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                removePlayer(team_id, btn, txt);
            }

        });

        alertDialogBuilder.setNeutralButton("Kembali", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }

        });

        AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 222) {
            if (resultCode == RESULT_OK) {
                // Get result from the result intent.
                String playerId = data.getStringExtra("ac_player_id");
                String playerNumber = data.getStringExtra("ac_player_number");
                String playerNick = data.getStringExtra("ac_player_nickname");
                String playerMode = data.getStringExtra("ac_player_mode");
                String playerTeam = data.getStringExtra("ac_player_team");

                List<String> list;

                if (checkTeam(playerTeam).equals("left")) {
                    list = kiriList;
                    selButton.setBackgroundResource(R.drawable.touch_green);
                } else {
                    list = kananList;
                    selButton.setBackgroundResource(R.drawable.touch_blue);
                }

                selButton.setText(playerNumber);

                selText.setText(playerNick);
                list.add(playerNumber);
                if (playerMode.equals("update")) {
                    String playerOld = data.getStringExtra("ac_player_old");
                    removeItem(list, playerOld);
                    dbConfig.updateFormation(match_id, playerTeam, playerOld, playerId, getPositionName(selButton));


                } else if (playerMode.equals("cadangan")) {
                    String playerOld = data.getStringExtra("ac_player_old");
                    removeItem(list, playerOld);
                    dbConfig.updateFormation(match_id, playerTeam, playerOld, playerId, getPositionName(selButton));
                } else {

                    dbConfig.insertFormation(match_id, playerTeam, playerId, getPositionName(selButton));
                    dbConfig.updateMatchStatus(match_id);
                }
                runSnack(playerNick + " (" + playerNumber + ")" + " berhasil dimasukkan");
                loadCadangan();
                //Log.e("d", zzz);
                // Do something with result...
            }
        } else if (requestCode == 333) {
            if (resultCode == RESULT_OK) {

                String data_player = data.getStringExtra("player");
                String data_team = data.getStringExtra("team_id");
                String goal_minute = data.getStringExtra("goal_minute");
                String post_time = data.getStringExtra("post_time");
                String exe_act = "goal";

                runSnack("+1 Goal untuk pemain nomor " + data_player);
                Vibrator vib = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                vib.vibrate(500);
                executeDetail(exe_act, data_team, data_player);
                String pid = dbConfig.getPlayerId(data_team, match_id, data_player);
                dbConfig.insertGoal(match_id, data_team, pid, goal_minute, post_time);

            }
        }
    }

    public void removeItem(List<String> list, String val) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(val)) {
                list.remove(i);

                break;
            }
        }
    }

    public String checkTeam(String id) {
        if (id.equals(team_a)) {
            return "left";
        } else {
            return "right";
        }
    }

    public String getPositionName(Button btn) {
        int btnIndex = -1;
        Button[] listButton = {btnLeft11, btnLeft21, btnLeft22, btnLeft23, btnLeft24, btnLeft31, btnLeft32, btnLeft33,
                btnLeft34, btnLeft41, btnLeft42, btnLeft43, btnLeft44, btnRight11, btnRight21, btnRight22, btnRight23, btnRight24, btnRight31, btnRight32, btnRight33, btnRight34,
                btnRight41, btnRight42, btnRight43, btnRight44};
        for (int i = 0; i < listButton.length; i++) {
            if (listButton[i] == btn) {
                btnIndex = i;
                break;
            }
        }
        String pos = "";
        if (btnIndex >= listPosition.length) {
            int lenIndex = listPosition.length;
            pos = listPosition[btnIndex - lenIndex];
        } else {
            pos = listPosition[btnIndex];
        }

        return pos;

    }

    public TextView getTextView(int index) {
        TextView[] listTextview = {txtLeft11, txtLeft21, txtLeft22, txtLeft23, txtLeft24, txtLeft31, txtLeft32,
                txtLeft33,
                txtLeft34, txtLeft41, txtLeft42, txtLeft43, txtLeft44, txtRight11, txtRight21, txtRight22, txtRight23, txtRight24, txtRight31, txtRight32, txtRight33, txtRight34,
                txtRight41, txtRight42, txtRight43, txtRight44};
        return listTextview[index];
    }

    public void fetchFormation(String team) {
        Button[] listButton = {btnLeft11, btnLeft21, btnLeft22, btnLeft23, btnLeft24, btnLeft31, btnLeft32, btnLeft33,
                btnLeft34, btnLeft41, btnLeft42, btnLeft43, btnLeft44, btnRight11, btnRight21, btnRight22, btnRight23, btnRight24, btnRight31, btnRight32, btnRight33, btnRight34,
                btnRight41, btnRight42, btnRight43, btnRight44};
        TextView[] listTextview = {txtLeft11, txtLeft21, txtLeft22, txtLeft23, txtLeft24, txtLeft31, txtLeft32,
                txtLeft33,
                txtLeft34, txtLeft41, txtLeft42, txtLeft43, txtLeft44, txtRight11, txtRight21, txtRight22, txtRight23, txtRight24, txtRight31, txtRight32, txtRight33, txtRight34,
                txtRight41, txtRight42, txtRight43, txtRight44};

        Cursor formation = dbConfig.getFormation(match_id, team);
        String pos = checkTeam(team);
        int PER_TEAM = (4 * 3) + 1;

        Log.e("action", action);

        while (formation.moveToNext()) {
            String player_id = formation.getString(formation.getColumnIndex("player_id"));
            String data_pos = formation.getString(formation.getColumnIndex("position"));
            String player_number = formation.getString(formation.getColumnIndex("player_number"));
            String player_nick = formation.getString(formation.getColumnIndex("nickname"));
            int POS_INDEX = getIndex(listPosition, data_pos);
            int index = -1;
            Log.e("LOG_FORM", player_id + " ==> " + player_nick + " => " + data_pos);
            if (!data_pos.equals("")) {
                if (pos.equals("left")) {
                    index = POS_INDEX;
                    if (action.equals("keeper")) {
                        if (data_pos.equals("11")) {
                            kiriList.add(player_number);
                        }
                    } else {
                        kiriList.add(player_number);
                    }


                } else {
                    index = PER_TEAM + POS_INDEX;


                    if (action.equals("keeper")) {
                        if (data_pos.equals("11")) {
                            kananList.add(player_number);
                        }
                    } else {
                        kananList.add(player_number);
                    }


                }

                Button playerButton = listButton[index];
                if (action.equals("keeper")) {
                    if (data_pos.equals("11")) {

                        TextView playerText = listTextview[index];
                        playerButton.setText(player_number);
                        playerText.setText(player_nick);

                    }
                } else {

                    TextView playerText = listTextview[index];
                    playerButton.setText(player_number);
                    playerText.setText(player_nick);
                }
                if (pos.equals("left")) {
                    if (action.equals("keeper")) {
                        if (data_pos.equals("11")) {
                            playerButton.setBackgroundResource(R.drawable.touch_green);
                        }
                    } else {
                        playerButton.setBackgroundResource(R.drawable.touch_green);
                    }
                } else {
                    if (action.equals("keeper")) {
                        if (data_pos.equals("11")) {
                            playerButton.setBackgroundResource(R.drawable.touch_blue);
                        }
                    } else {
                        playerButton.setBackgroundResource(R.drawable.touch_blue);
                    }

                    runSnack("Formasi kedua tim telah berhasil disusun.");
                }

            }
        }


    }

    public int getIndex(String[] list, String data) {
        int index = -1;
        for (int i = 0; i < list.length; i++) {
            if (list[i].equals(data)) {
                index = i;
                break;
            }
        }
        return index;
    }

    public int getIndexButton(Button button) {
        int index = -1;
        Button[] listButton = {btnLeft11, btnLeft21, btnLeft22, btnLeft23, btnLeft24, btnLeft31, btnLeft32, btnLeft33,
                btnLeft34, btnLeft41, btnLeft42, btnLeft43, btnLeft44, btnRight11, btnRight21, btnRight22, btnRight23, btnRight24, btnRight31, btnRight32, btnRight33, btnRight34,
                btnRight41, btnRight42, btnRight43, btnRight44};
        for (int i = 0; i < listButton.length; i++) {
            if (listButton[i] == button) {
                index = i;
                break;
            }
        }
        return index;
    }

    public int getIndexTextView(TextView[] list, String data) {
        int index = -1;
        for (int i = 0; i < list.length; i++) {
            if (list[i].equals(data)) {
                index = i;
                break;
            }
        }
        return index;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_match, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        Intent menuIntent;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_result) {
            menuIntent = new Intent(getApplicationContext(), MatchDetailActivity.class);
            menuIntent.putExtra("match_name", match_name);
            menuIntent.putExtra("match_id", match_id);
            menuIntent.putExtra("action_name", action_name);
            menuIntent.putExtra("action", action);
            menuIntent.putExtra("team_a", team_a);
            menuIntent.putExtra("team_b", team_b);
            startActivity(menuIntent);
            return true;
        } else if (id == R.id.action_export) {
            if (action.equals("sgc")) {
                openSotCSV();
            } else {
                exportCSV(null);
            }

        } else if (id == R.id.action_field) {

            if (boolMode == 1) {
                boolMode = 0;
            } else {
                boolMode = 1;
            }

            setupFieldMode(boolMode);
        } else if (id == R.id.action_export_online) {
            String json = jsonExport.exportOnline(this, match_id, action);
            sync(json);
            Log.e("json", json);
        } else if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void exportCSV(String act) {
        try {
        DataExport dataExport = new DataExport();

        verifyStoragePermissions(this);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String timestamp_str = String.valueOf(timestamp.getTime());
        File dbFile = getDatabasePath("_fs_db.db");
        DBConfig dbhelper = new DBConfig(getApplicationContext());
        File exportDir = new File(Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "");
        if (!exportDir.exists()) {
            exportDir.mkdirs();
        }
        String fileName = "Export " + action_name + " -" + timestamp_str + ".csv";
        File file = new File(exportDir, fileName);
        Uri uri = Uri.fromFile(file);
        //Log.e("URL PATH", String.valueOf(uri));

            file.createNewFile();
            CSVWriter csvWrite = new CSVWriter(new FileWriter(file));
            SQLiteDatabase db = dbhelper.getReadableDatabase();
            String customAct;
            if (act != null) {
                if (act.equals("goaltiming")) {
                    customAct = "goal";
                } else {
                    customAct = action;
                }
            } else {
                customAct = action;
            }

            Cursor curCSV = dataExport.exportCSV(getApplicationContext(), match_id, customAct);
            csvWrite.writeNext(curCSV.getColumnNames());
            while (curCSV.moveToNext()) {
                int colCount = curCSV.getColumnCount();
                Log.e("colcount", String.valueOf(colCount));
                //Which column you want to exprort
                String[] arrStr = new String[colCount];

                for (int i = 0; i < colCount; i++) {
                    arrStr[i] = curCSV.getString(i);
                }

                csvWrite.writeNext(arrStr);
            }
            csvWrite.close();
            curCSV.close();
            runSnack("File " + fileName + " berhasil diexport. File ini berlokasi di folder Download.");
        } catch (Exception sqlEx) {
            Log.e("MainActivity", sqlEx.getMessage(), sqlEx);
        }
    }

    public boolean getNetworkAvailability() {
        return Utils.isNetworkAvailable(getApplicationContext());
    }


    private void sync(String data) {
        mDialog = new ProgressDialog(this);
        if (getNetworkAvailability()) {

            mDialog = new ProgressDialog(MatchActivity.this);
            mDialog.setMessage("Exporting Data..");
            mDialog.setCancelable(false);
            mDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            mDialog.show();

            Call<String> listCall = null;
            if (action.equals("touch")) {
                listCall = mManager.getDataService().syncTouch(data);
            } else if (action.equals("fouls_passing")) {
                listCall = mManager.getDataService().syncFalsePassing(data);
            } else if (action.equals("fouls_control")) {
                listCall = mManager.getDataService().syncFalseControl(data);
            } else if (action.equals("sgc")) {
                listCall = mManager.getDataService().syncOtherValue(data);
            } else if (action.equals("keeper")) {
                listCall = mManager.getDataService().syncGoalKeeper(data);
            }
            Log.e("url", String.valueOf(listCall.request().url()));
            listCall.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, Response<String> response) {

                    if (response.isSuccessful()) {
                        String resp = response.body();
                        if (resp.equals("1")) {
                            runSnack("Data berhasil diexport.");
                        } else {
                            runSnack("Ooops! Data gagal diexport ke sistem.");
                        }

                    } else {

                        int sc = response.code();
                        switch (sc) {
                            case 400:
                                Log.e("Error 400", "Bad Request");
                                break;
                            case 404:
                                Log.e("Error 404", "Not Found");
                                break;
                            default:
                                Log.e("Error", "Generic Error");
                        }

                        Toast.makeText(getApplicationContext(), "Failed to export. Error code: " + sc, Toast.LENGTH_LONG)
                                .show();


                    }

                    mDialog.dismiss();

                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    mDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
                }


            });

        } else {
            mDialog.dismiss();
            Toast.makeText(getApplicationContext(), "Mohon aktifkan koneksi internet anda.", Toast.LENGTH_LONG).show();
        }

    }


    public void fieldMode(Boolean b) {
        if (b) {
            editMode = false;
            llField.setBackgroundResource(R.drawable.field);
            curCadanganNickname = "";
            curCadanganTeam = "";
            curCadanganNo = null;
        } else {
            editMode = true;
            llField.setBackgroundResource(R.color.colorGreyDark);
        }
    }


    @Override
    public void onClick(int position) {
        Player curPlayer = cadanganAdapter1.getSelectedItem(position);
        fieldMode(editMode);
        curCadanganNo = curPlayer.getPlayer_number();
        curCadanganNickname = curPlayer.getNickname();
        curCadanganTeam = team_a;
        if (editMode) {
            runSnack("Silahkan pilih pemain yang ingin diganti.");
        }
    }

    @Override
    public void onClick2(int position) {

        Player curPlayer = cadanganAdapter2.getSelectedItem(position);
        fieldMode(editMode);
        curCadanganNo = curPlayer.getPlayer_number();
        curCadanganNickname = curPlayer.getNickname();
        curCadanganTeam = team_b;
        if (editMode) {
            runSnack("Silahkan pilih pemain yang ingin diganti.");
        }

    }
}

package com.stikombali.fundamentalachievements;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.facebook.stetho.Stetho;

public class MainActivity extends AppCompatActivity {

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    LinearLayout menuTeam, menuSentuhan, menuMatch, menuPassing, menuControl, menuShoot, menuSetting, menuGK, menuAbout;
    Intent menuIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Stetho.initializeWithDefaults(this);

        menuTeam = (LinearLayout) findViewById(R.id.menuTeam);
        menuTeam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "team");
                startActivity(menuIntent);
            }
        });

        menuSentuhan = (LinearLayout) findViewById(R.id.menuSentuhan);
        menuSentuhan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "touch");
                startActivity(menuIntent);
            }
        });

        menuPassing = (LinearLayout) findViewById(R.id.menuPassing);
        menuPassing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "fouls_passing");
                startActivity(menuIntent);
            }
        });

        menuControl = (LinearLayout) findViewById(R.id.menuControl);
        menuControl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "fouls_control");
                startActivity(menuIntent);
            }
        });

        menuShoot = (LinearLayout) findViewById(R.id.menuShoot);
        menuShoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "sgc");
                startActivity(menuIntent);
            }
        });

        menuSetting = (LinearLayout) findViewById(R.id.menuSetting);
        menuSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), SettingActivity.class);
                startActivity(menuIntent);
            }
        });

        menuAbout = (LinearLayout) findViewById(R.id.menuAbout);
        menuAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), AboutActivity.class);
                startActivity(menuIntent);
            }
        });

        menuGK = (LinearLayout) findViewById(R.id.menuGK);
        menuGK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "keeper");
                startActivity(menuIntent);
            }
        });

        menuMatch = (LinearLayout) findViewById(R.id.menuMatch);
        menuMatch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuIntent = new Intent(getApplicationContext(), TurnamenActivity.class);
                menuIntent.putExtra("action", "match");
                startActivity(menuIntent);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

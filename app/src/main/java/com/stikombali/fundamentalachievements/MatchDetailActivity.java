package com.stikombali.fundamentalachievements;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MatchDetailActivity extends AppCompatActivity {

    Integer NUM_ROW = 20;
    String tr = "", th;
    String match_id, match_name, team_a, team_b, action, action_name;

    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        team_a = getIntent().getStringExtra("team_a");
        team_b = getIntent().getStringExtra("team_b");

        action = getIntent().getStringExtra("action");
        match_id = getIntent().getStringExtra("match_id");
        match_name = getIntent().getStringExtra("match_name");
        action_name = getIntent().getStringExtra("action_name");
        getSupportActionBar().setTitle("Statistik " + action_name);
        getSupportActionBar().setSubtitle(match_name);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


    }


    private void setupViewPager(ViewPager viewPager) {

        final ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());


        Bundle bundleA = new Bundle();
        bundleA.putString("type", action);
        bundleA.putString("team", team_a);
        bundleA.putString("match_id", match_id);
        FragmentResultTeam teamA = new FragmentResultTeam();
        teamA.setArguments(bundleA);
        adapter.addFragment(teamA, "TEAM A");

        FragmentResultTeam teamB = new FragmentResultTeam();
        teamB.setArguments(bundleA);

        Bundle bundle = new Bundle();
        bundle.putString("team", team_b);
        bundle.putString("type", action);
        bundle.putString("match_id", match_id);
        FragmentResultTeam fragB = new FragmentResultTeam();
        fragB.setArguments(bundle);


        adapter.addFragment(fragB, "TEAM B");
        adapter.notifyDataSetChanged();
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}

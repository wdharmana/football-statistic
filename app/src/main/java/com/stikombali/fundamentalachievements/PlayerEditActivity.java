package com.stikombali.fundamentalachievements;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.stikombali.fundamentalachievements.database.DBConfig;

public class PlayerEditActivity extends AppCompatActivity {

    public CoordinatorLayout coordinatorLayout;
    String player_id, player_name, player_nickname, player_number;
    EditText edtName, edtNickname, edtPlayerNumber;
    Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player_edit);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);


        player_id = getIntent().getStringExtra("player_id");
        player_name = getIntent().getStringExtra("player_name");
        player_nickname = getIntent().getStringExtra("player_nickname");
        player_number = getIntent().getStringExtra("player_number");

        getSupportActionBar().setTitle("Edit Player");
        getSupportActionBar().setSubtitle(player_name);

        edtName = (EditText) findViewById(R.id.edtName);
        edtNickname = (EditText) findViewById(R.id.edtNickname);
        edtPlayerNumber = (EditText) findViewById(R.id.edtPlayerNumber);

        btnUpdate = (Button) findViewById(R.id.btnUpdate);

        setupData();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData();
            }
        });


    }

    private void updateData() {
        String data_name, data_nickname, data_number;
        data_name = edtName.getText().toString();
        data_nickname = edtNickname.getText().toString();
        data_number = edtPlayerNumber.getText().toString();

        DBConfig dbConfig = new DBConfig(this);
        Boolean res = dbConfig.updatePlayer(player_id, data_name, data_nickname, data_number);
        if (res) {
            runSnack("Data berhasil disimpan");
            getSupportActionBar().setSubtitle(data_name);
        } else {
            runSnack("Gagal melakukan proses update");
        }
    }

    public void runSnack(String msg) {
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, msg, Snackbar.LENGTH_LONG);

        snackbar.show();
    }

    private void setupData() {
        edtName.setText(player_name);
        edtNickname.setText(player_nickname);
        edtPlayerNumber.setText(player_number);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}

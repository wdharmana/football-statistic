package com.stikombali.fundamentalachievements;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.stikombali.fundamentalachievements.database.DBConfig;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentResultTeam extends Fragment {

    int NUM_ROW;
    String tr = "", th;
    DBConfig dbConfig;
    String type, match_id, team;

    public FragmentResultTeam() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        type = args.getString("type");
        match_id = args.getString("match_id");
        team = args.getString("team");

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.content_match_detail, container, false);


        dbConfig = new DBConfig(getActivity());

        Cursor dt = dbConfig.getStats(type, match_id, team);
        NUM_ROW = dt.getCount();
        Log.e("NUM_ROW", String.valueOf(NUM_ROW));
        Log.e("MATCH_DETAIL", String.valueOf(dbConfig.getStatCount()));

        WebView webView = (WebView) rootView.findViewById(R.id.webView);

        while (dt.moveToNext()) {

            if (type.equals("sgc")) {
                String goal = dt.getString(dt.getColumnIndex("goal"));
                String sot = dt.getString(dt.getColumnIndex("sot"));
                String fouls = dt.getString(dt.getColumnIndex("fouls"));
                String ycard = dt.getString(dt.getColumnIndex("yellow_card"));
                String rcard = dt.getString(dt.getColumnIndex("red_card"));

                if (!goal.equals("0") || !sot.equals("0") || !fouls.equals("0") || !ycard.equals("0") | !rcard.equals("0")) {
                    tr += "<tr><td>" + dt.getString(dt.getColumnIndex("player_number")) + "</td><td>" + dt.getString(dt
                            .getColumnIndex("name")) + "</td><td>" + dt.getString(dt
                            .getColumnIndex("account_number")) + "</td>";
                    tr += "<td style='text-align:right;'>" + sot + "</td>" + "<td style='text-align:right;" +
                            "'>" + goal + "</td>" + "<td style='text-align:right;" +
                            "'>" + fouls + "</td>" + "<td style='text-align:right;" +
                            "'>" + ycard + "</td>" + "<td style='text-align:right;" +
                            "'>" + rcard + "</td>" + "</tr>";
                }

            } else if (type.equals("keeper")) {
                //String reaction = dt.getString(dt.getColumnIndex("reaction"));
                String saving = dt.getString(dt.getColumnIndex("saving"));
                String distribution = dt.getString(dt.getColumnIndex("distribution"));
                String anticipation = dt.getString(dt.getColumnIndex("anticipation"));

                if (!saving.equals("0") || !distribution.equals("0") || !anticipation.equals("0")) {
                    tr += "<tr><td>" + dt.getString(dt.getColumnIndex("player_number")) + "</td><td>" + dt.getString(dt
                            .getColumnIndex("name")) + "</td><td>" + dt.getString(dt
                            .getColumnIndex("account_number")) + "</td>";
                    tr += "<td style='text-align:right;" +
                            "'>" + saving + "</td>" + "<td style='text-align:right;" +
                            "'>" + distribution + "</td>" + "<td style='text-align:right;" +
                            "'>" + anticipation + "</td>" + "</tr>";
                }

            } else {
                tr += "<tr><td>" + dt.getString(dt.getColumnIndex("player_number")) + "</td><td>" + dt.getString(dt
                        .getColumnIndex("name")) + "</td><td>" + dt.getString(dt
                        .getColumnIndex("account_number")) + "</td>";
                tr += "<td style='text-align:right;'>" + dt.getString(dt
                        .getColumnIndex("jumlah")) + "</td></tr>";
            }


        }
        if (type.equals("sgc")) {
            th = "<tr><th rowspan='2'>NO</th><th rowspan='2'>NAMA LENGKAP</th><th " +
                    "rowspan='2'>NISN</th><th rowspan='2'>SHOOT</th><th rowspan='2'>GOAL</th><th " +
                    "rowspan='2'>FOULS</th><th colspan='2'>KARTU</th></tr>";
            th += "<tr><th>KUNING</th><th>MERAH</th></tr>";
        } else if (type.equals("keeper")) {
            th = "<tr><th >NO</th><th >NAMA LENGKAP</th><th " +
                    ">NISN</th><th >SAVING</th><th >DISTRIBUTION</th><th " +
                    ">GOAL</th></tr>";
        } else {
            th = "<tr><th>NOMOR</th><th>NAMA LENGKAP</th><th>NISN</th><th>JUMLAH</th></tr>";
        }

        String tableGoal = "", th_goal = "", tr_goal = "";
        /* check goal */
        if (type.equals("sgc")) {
            Cursor c = dbConfig.getGoal(match_id, team);
            if (c.getCount() > 0) {
                th_goal = "<tr><th>NO</th><th>NAMA LENGKAP</th><th>MENIT</th></tr>";
                while (c.moveToNext()) {
                    tr_goal += "<tr><td>" + c.getString(c.getColumnIndex("player_number")) + "</td><td>" + c.getString(c
                            .getColumnIndex("name")) + "</td><td>" + c.getString(c
                            .getColumnIndex("minute")) + "</td></tr>";
                }

                tableGoal = "<h4 style='margin-bottom:5px'>Informasi Goal</h4><table " +
                        "class='tg'>" + th_goal + tr_goal + "</table>";
            }
        }


        String summary = "<style type='text/css'>table{width:100%;} th{background:#ddd;}.tg{border-collapse:collapse;" +
                "border-spacing:0;}" +
                ".tg " +
                "td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}.tg .tg-yw4l{vertical-align:top}</style><table class='tg'> " +
                th + tr +
                "</table>" + tableGoal;
        webView.loadData(summary, "text/html; charset=utf-8", "utf-8");

        return rootView;
    }


}
